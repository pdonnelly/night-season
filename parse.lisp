;;; Night Season, a stealth adventure game
;;; Copyright 2019 Paul Donnelly
;;;
;;; This file is part of Night Season.
;;;
;;; Night Season is free software: you can redistribute it and/or modify it
;;; under the terms of the GNU Affero General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.
;;;
;;; Night Season is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
;;; Affero General Public License for more details.
;;;
;;; You should have received a copy of the GNU Affero General Public
;;; License along with Night Season. If not, see
;;; <https://www.gnu.org/licenses/>.

(in-package :night-season)

(defun scan-words (string split-fn)
  "Split a string into series of strings wherever SPLIT-FN is true for that char."
  (declare (optimizable-series-function))
  (let* ((split-here-p (map-fn t split-fn (scan 'string string)))
         (end (catenate (positions split-here-p)
                        (scan (list (length string)))))
         (start (previous (#M1+ end) 0))
         (word (subseq string start end)))
    (values
     (choose (plusp (length word))
             word))))

(defun symbolify (string)
  (collect 'vector
           (let ((w (scan-words string
                                (lambda (c)
                                  (member c '(#\space #\,))))))
             (or (parse-integer w :junk-allowed t)
                 (intern (string-upcase w) "NIGHT-SEASON")))))

(defun match-name (command-line name-words &optional (start 0))
  "Given a COMMAND-LINE sequence and a list of NAME-WORDS which may make up a name, and an optional START offset in the command line, return (VALUES <the longest run of words that match a word in NAME-WORDS> <the index in COMMAND-LINE of the word after the last match>)."
  (let ((matches (until-if (lambda (in) (not (member in name-words)))
                           (subseries (scan command-line) start))))
    (values (collect matches)
            (+ start (collect-length matches)))))

(defstruct (command (:constructor make-command (name grammars)))
  name grammars)

(defun match-some-name (domain command-line start)
  (dolist (i domain)
    (destructuring-bind (name-words . obj) i
      (multiple-value-bind (match-words new-index)        
          (match-name command-line name-words start)
        (when match-words
          (return (values match-words obj new-index)))))))

(defun parse-grammar (actor grammar command-line)
  "See if the given GRAMMAR can match the COMMAND-LINE. Returns a boolean success, a string it could match, and a list of objects with one item for each of the grammar's items that call for an object."
  (let ((matched '())
        (args '())
        (i 0))

    (labels ((r (fully-matched)
               (return-from parse-grammar (values fully-matched
                                                  (nreverse matched)
                                                  (nreverse args)))))
      (when (or (null command-line)
                (zerop (length command-line)))
        (r nil))

      (iterate ((w (scan grammar)))
        (etypecase w
          (symbol (unless (and (< i (length command-line))
                               (if (eq w :nnn)
                                   (numberp (elt command-line i))
                                   (eq w (elt command-line i))))
                    (r nil))
                  (push (elt command-line i) matched)
                  (if (numberp (elt command-line i))
                      (push (elt command-line i) args))
                  (incf i))
          (function
           (multiple-value-bind (match-words obj new-i)
               (match-some-name (funcall w actor) command-line i)
             (unless match-words (r nil))
             (setf i new-i)
             (setf matched (append (nreverse match-words) matched))
             (push obj args)))))
      (r (= i (length command-line))))))

(defun parse-command (actor command command-line)
  "See if any of the grammars associated with a command can match the command line. Returns a boolean success, a string it could match, and a list of objects with one item for each of the grammar's items that call for an object."
  (loop
     with fully-matched = nil
     with best-match = nil
     with args = nil
     until fully-matched 
     for g in (command-grammars command)
     do
     ;; What we're trying to do is track a decent partial match so
     ;; that if we can't get a full one, we can at least say we
     ;; understood up to something or other.
       (multiple-value-bind (full most-recent-match things)
           (parse-grammar actor g command-line)
         (setf fully-matched full)
         (when most-recent-match
           (setf best-match most-recent-match
                 args things)))
     finally (return (values fully-matched best-match args))))

;; Note that the "things" or "objects" or "args" being returned is a list with one item for each argument place in the grammar.

(defun parse (commands actor string)
  "Parse command string. Return a list of the matched command and the
parsed arguments to it, or a failure message string."
  (let ((best-match nil)
        (fully-matched-command nil)
        (args nil)
        (command-line (and string (symbolify string))))
    (loop
      for c being the hash-value in commands
      until fully-matched-command
      do (multiple-value-bind (full match things)
             (parse-command actor c command-line)
           (setf best-match (or match best-match)
                 args things)
           (when full (setf fully-matched-command c))))
    (if fully-matched-command
        (list (command-name fully-matched-command) args)
        (if (null best-match)
            (message nil actor 'understood-nothing)
            (with-output-to-string (*standard-output*)
              ;;(break)
              (message t actor 'understood-up-to)
              (iterate ((s (string-downcase (symbol-name (scan best-match)))))
                (format t " ~a" s))
              (format t "... "))))))
