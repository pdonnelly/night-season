;;; Night Season, a stealth adventure game
;;; Copyright 2019 Paul Donnelly
;;;
;;; This file is part of Night Season.
;;;
;;; Night Season is free software: you can redistribute it and/or modify it
;;; under the terms of the GNU Affero General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.
;;;
;;; Night Season is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
;;; Affero General Public License for more details.
;;;
;;; You should have received a copy of the GNU Affero General Public
;;; License along with Night Season. If not, see
;;; <https://www.gnu.org/licenses/>.

(in-package :night-season)

(defun blocks-fov-p (pos)
  "Can one see through POS?"
  (collect-first (choose-if #'opaquep
                            (scan-body@ pos))))

(defun cube-fov (body)
  "Calculate FOV for BODY, marking all cells within a cube centered on
the player as visible and storing the result in body's fov slot."
  (let* ((dims (zone-dimensions (pos-zone (pos body))))
         (radius 5)
         (-radius (- radius))
         (output (setf (fov body) (make-array dims :element-type 'bit))))
    (iterate ((p (scan-cube
                  (clip-pos (shift body :z -radius :y -radius :x -radius))
                  (shift
                   (clip-pos (shift body :z radius :y radius :x radius))
                   :z 1 :y 1 :x 1)
                  t)))
      (setf (pos-aref output p) 1))
    output))

(defun calculate-fov (body)
  "Calculate FOV for BODY, marking cells visible according to the
player's line-of-sight and storing the result in body's fov slot."
  (declare (optimize (speed 3)))
  (let* ((dims (zone-dimensions (pos-zone (pos body))))
         (output (setf (fov body) (make-array dims :element-type 'bit)))
         (obstructions (make-array dims :element-type 'bit)))
    (iterate ((p (scan-zone (pos-zone (pos body)) nil t)))
      (setf (pos-aref obstructions p)
            (if (blocks-fov-p p) 1 0)))
    (whole-cube-fov obstructions
                    (pos-x (pos body)) (pos-y (pos body)) (pos-x (pos body))
                    output)
    output))

(defun %scan-wedge (near-distance far-distance)
  (declare (optimizable-series-function 3))
  (scan-fn '(values integer integer integer)
           (lambda ()
             (values near-distance 0 0))
           (lambda (z y x)
             (incf x)
             (when (> x z) (setf x 0) (incf y))
             (when (> y z) (setf y 0) (incf z))
             (values z y x))
           (lambda (z y x)
             (declare (ignore y x))
             (>= z far-distance))))

(defun %scan-wedge-bounded (near depth height width)
  (declare (optimizable-series-function 3)
           (optimize (speed 3)))
  (assert (not (minusp depth)))
  (assert (not (minusp height)))
  (assert (not (minusp width)))
  (scan-fn '(values fixnum fixnum fixnum)
           (lambda ()
             (values near 0 0))
           (lambda (z y x)
             (incf x)
             (when (or (> x z)
                       (>= x width))
               (setf x 0) (incf y))
             (when (or (> y z)
                       (>= y height))
               (setf y 0) (incf z))
             (values z y x))
           (lambda (z y x)
             (declare (ignore y x))
             (>= z depth))))

(defun %scan-rectangle (miny maxy minx maxx)
  (declare (optimizable-series-function 2))
  (scan-fn '(values fixnum fixnum)
           (lambda ()
             (values miny minx))
           (lambda (y x)
             (incf x)
             (when (>= x maxx) (setf x minx) (incf y))
             (values y x))
           (lambda (y x)
             (declare (ignore x))
             (>= y maxy))))

(defun %fov-wedge-fn (input output
                      z0 y0 x0 ; origin
                      dz dy dx ; direction
                      nz ny nx); axis permutation
  (declare (optimize (speed 3)))
  (check-type input (simple-array bit))
  (check-type output (simple-array bit))
  (check-type z0 fixnum)
  (check-type y0 fixnum)
  (check-type x0 fixnum)
  (check-type dz (integer -1 1))
  (check-type dy (integer -1 1))
  (check-type dx (integer -1 1))
  (check-type nz (integer 0 2))
  (check-type ny (integer 0 2))
  (check-type nx (integer 0 2))
  (labels ((dim (n) (array-dimension input n))
           (clip (n max)
             (cond ((minusp (the fixnum n)) 0)
                   ((> n max) max)
                   (t n)))
           (slope (h r d)
             (let ((product (* h r d)))
               (truncate (clip (truncate product) h)))) )
    (let* ((ratio 4)
           (h (* ratio (dim ny)))
           (w (* ratio (dim nx)))
           (hf (* h 1.0))
           (wf (* w 1.0))
           (mask (make-array (list h w) :element-type 'bit))
           (coord (make-array 3 :element-type 'fixnum))
           (mz (if (minusp dz) z0 (- (dim nz) z0)))
           (my (if (minusp dy) y0 (- (dim ny) y0)))
           (mx (if (minusp dx) x0 (- (dim nx) x0))))
      (flet ((body (depth down right)
               (setf (elt coord nz) (+ z0 (* dz depth))
                     (elt coord ny) (+ y0 (* dy down))
                     (elt coord nx) (+ x0 (* dx right)))
               (let* ((r (/ 1.0 depth))
                      (slope1min (slope hf r (- down .5)))
                      (slope1max (slope hf r (+ down .5)))
                      (slope2min (slope wf r (- right .5)))
                      (slope2max (slope wf r (+ right .5))))
                 (when (loop
                         for s1 fixnum from slope1min below slope1max
                         thereis
                         (loop for s2 fixnum from slope2min below slope2max
                               thereis (zerop (aref mask s1 s2))))
                   (setf (aref output
                               (elt coord 0)
                               (elt coord 1)
                               (elt coord 2))
                         1)
                   (when (= 1 (aref (the (simple-array bit (* * *)) input)
                                    (elt coord 0)
                                    (elt coord 1)
                                    (elt coord 2)))
                     (loop
                       for s1 from slope1min below slope1max
                       do
                          (loop for s2 from slope2min below slope2max
                                do (setf (aref mask s1 s2) 1))))))))
        (iterate (((depth down right)
                   (%scan-wedge-bounded 1 mz my mx)))
          (body depth down right)))
      (values))))

(defun whole-cube-fov (input z y x
                       &optional
                         (output (make-array (array-dimensions input)
                                             :element-type 'bit)))
  (declare (optimize (speed 3)))
  (check-type output (simple-array bit))
  (check-type input (simple-array bit))
  (assert (equal (array-dimensions input)
                 (array-dimensions output)))
  (setf (aref output z y x) 1)

  (dolist (dz '(1 -1))
    (dolist (dy '(1 -1))
      (dolist (dx '(1 -1))
        (%fov-wedge-fn input output
                       z y x
                       dz dy dx
                       0 1 2)
        (%fov-wedge-fn input output
                       z y x
                       dz dy dx
                       1 0 2)
        (%fov-wedge-fn input output
                       z y x
                       dz dy dx
                       2 0 1))))
  output)

(defun test-cube-fov ()
  (let (o)
    (time
     (setf o (whole-cube-fov
              #3A(((0 0 0 0 0) (0 0 0 0 0) (0 0 0 0 0) (0 0 0 0 0) (0 0 0 0 0))
                  ((0 0 0 0 0) (0 0 0 0 0) (0 0 0 0 0) (0 0 0 0 0) (0 0 0 0 0))
                  ((0 0 0 0 0) (0 1 0 0 0) (0 0 0 0 0) (0 0 0 0 0) (0 0 0 0 0))
                  ((0 0 0 0 0) (0 0 0 0 0) (0 1 0 0 0) (0 0 0 0 0) (0 0 0 0 0))
                  ((0 0 0 0 0) (0 0 0 0 0) (0 0 0 0 0) (0 0 0 0 0) (0 0 0 0 0)))
              2 2 2)))
    o))
