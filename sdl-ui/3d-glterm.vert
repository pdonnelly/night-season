#version 330 core
layout (location = 0) in vec2 quad_vertex;
layout (location = 1) in float char;
layout (location = 2) in vec3 ifg_color;
layout (location = 3) in vec3 ibg_color;

out vec3 fg_color;
out vec3 bg_color;
out vec3 texcoord;

uniform mat4 rotation;
uniform ivec3 map_dims;

#define COLS 80;
#define ROWS 24;

void main()
{
    // We assume that the screen is ROWS by COLS, therefore the
    // on-screen dimensions of a character are thus.
    float w = 4.0/COLS;
    float h = 4.0/ROWS;
    vec2 char_dims = vec2(w,h);
    ivec3 d = map_dims.zyx;

    mat4 quarter_view;
    quarter_view[0] = vec4(1,0,0,0);
    quarter_view[1] = vec4(0,.707372,.9974950,0);
    quarter_view[2] = vec4(0,-.9974950,.0707372,0);
    quarter_view[3] = vec4(0,0,0,1);

    int y = (gl_InstanceID / (d.x*d.y)) - (d.z/2);
    int remainder = gl_InstanceID % (d.x*d.y);
    int z = (remainder / d.y) - (d.y/2);
    int x = (gl_InstanceID % d.x) - (d.x/2);
    vec4 pos = vec4(x*0.5, -y*0.5, z*0.5, 1);
    vec4 rotated = rotation*pos;
    
    gl_Position = quarter_view*(rotated*vec4(w*2,h*2,w*2,1))
                  +vec4(quad_vertex.x*char_dims.x,quad_vertex.y*-char_dims.y,0,1)
                  +vec4(0.4,-0.9,0,0);

    float z_fade = (1-(pos.z+1)/2);
    fg_color = vec3(ifg_color.x*z_fade, ifg_color.y, ifg_color.z*(1-z_fade));
    bg_color = ibg_color;
    texcoord = vec3(quad_vertex,char);
}
