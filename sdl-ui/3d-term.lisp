;;; Night Season, a stealth adventure game
;;; Copyright 2019 Paul Donnelly
;;;
;;; This file is part of Night Season.
;;;
;;; Night Season is free software: you can redistribute it and/or modify it
;;; under the terms of the GNU Affero General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.
;;;
;;; Night Season is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
;;; Affero General Public License for more details.
;;;
;;; You should have received a copy of the GNU Affero General Public
;;; License along with Night Season. If not, see
;;; <https://www.gnu.org/licenses/>.

(in-package :night-season)

(defstruct (3d-glterm (:constructor %make-3d-glterm))
  width
  height
  depth
  contents-glarray
  contents-buffer
  program)

(defun 3d-terminal-char-count (term)
  (* (3d-glterm-width term)
     (3d-glterm-height term)
     (3d-glterm-depth term)))

(defun 3d-terminal-set (term i char &optional (fg '(1 1 1)) (bg '(0 0 0)))
  (when char
    (let ((a (3d-glterm-contents-glarray term))
          (o (* 7 i)))
      (assert (< -1 o (- (gl:gl-array-byte-size a)
                         6)))
      (destructuring-bind (fr fg fb) fg
        (destructuring-bind (br bg bb) (or bg '(0 0 0))
          (setf (gl:glaref a o) (* 1.0 (char-code char))
                (gl:glaref a (+ o 1)) (* 1.0 fr)
                (gl:glaref a (+ o 2)) (* 1.0 fg)
                (gl:glaref a (+ o 3)) (* 1.0 fb)
                (gl:glaref a (+ o 4)) (* 1.0 br)
                (gl:glaref a (+ o 5)) (* 1.0 bg)
                (gl:glaref a (+ o 6)) (* 1.0 bb)))))))

(defun 3d-terminal-clear (term)
  (iterate ((i (scan-range :below (3d-terminal-char-count term))))
    (3d-terminal-set term i #\space)))


(defun 3d-terminal-set-vertex-attribs (term)
  (gl:bind-buffer :array-buffer (3d-glterm-contents-buffer term))
  (gl:vertex-attrib-pointer 1 1 :float nil 28 0)  ; char index
  (gl:vertex-attrib-pointer 2 3 :float nil 28 4)  ; fg color
  (gl:vertex-attrib-pointer 3 3 :float nil 28 16) ; bg color
  (%gl:vertex-attrib-divisor 1 1)
  (%gl:vertex-attrib-divisor 2 1)
  (%gl:vertex-attrib-divisor 3 1)
  (gl:enable-vertex-attrib-array 1)
  (gl:enable-vertex-attrib-array 2)
  (gl:enable-vertex-attrib-array 3))

(defun 3d-terminal-draw (term)
  (gl:use-program (3d-glterm-program term))
  (let ((now (/ (get-internal-real-time)
                internal-time-units-per-second
                2)))
    (gl:uniform-matrix (gl:get-uniform-location (3d-glterm-program term) "rotation")
                       4
                       (vector (vector (cos now) 0  (- (sin now)) 0    ; 1
                                       0         1  0             0    ; 2
                                       (sin now) 0  (cos now)     0    ; 0
                                       0         0  0             1))  ; 1
                       nil))
  (%gl:named-buffer-sub-data
   (3d-glterm-contents-buffer term)
   0
   (gl::gl-array-byte-size (3d-glterm-contents-glarray term))
   (gl::gl-array-pointer (3d-glterm-contents-glarray term)))
  (3d-terminal-set-vertex-attribs term)
  (gl:draw-arrays-instanced :triangles 0 8 (3d-terminal-char-count term)))

(defun compile-3d-term (dims)
  (let ((vshader (gl:create-shader :vertex-shader))
        (fshader (gl:create-shader :fragment-shader))
        (program (gl:create-program)))
    (gl:shader-source vshader (read-shader-source-file "sdl-ui/3d-glterm.vert"))
    (gl:compile-shader vshader)
    (gl:shader-source fshader (read-shader-source-file "sdl-ui/3d-glterm.frag"))
    (gl:compile-shader fshader)
    (let ((i (gl:get-shader-info-log fshader)))
      (when (plusp (length i)) (print i)))
    (let ((i (gl:get-shader-info-log vshader)))
      (when (plusp (length i)) (print i)))
    (gl:attach-shader program vshader)
    (gl:attach-shader program fshader)
    (gl:link-program program)
    (gl:delete-shader vshader)
    (gl:delete-shader fshader)
    (gl:use-program program)
    (gl:uniform-matrix (gl:get-uniform-location program "rotation")
                       4
                       (vector (vector 1 0 0 0
                                       0 1 0 0
                                       0 0 1 0
                                       0 0 0 1))
                       nil)
    (gl:uniformiv (gl:get-uniform-location program "map_dims")
                  (make-array 3 :initial-contents dims))
    (gl:uniformi (gl:get-uniform-location program "char_textures") 0)
    program))

(defun make-3d-glterm (dims)
  ;; We assume the regular glterm has already been initialized and we
  ;; can use its stuff.
  (let ((term (%make-3d-glterm :depth (first dims) :height (second dims) :width (third dims))))
    (setf (3d-glterm-program term) (compile-3d-term dims))
    
    (let ((b (gl:gen-buffer)))
      (setf (3d-glterm-contents-buffer term) b)
      (3d-terminal-set-vertex-attribs term)
      (%gl::named-buffer-data b (* (3d-terminal-char-count term)
                                   7
                                   4)
                              (cffi:null-pointer)
                              :stream-draw))
    (setf (3d-glterm-contents-glarray term)
          (gl:alloc-gl-array :float (* (3d-terminal-char-count term)
                                       (+ 1 ; char index
                                          3 ; fg color
                                          3 ; bg color
                                          ))))
    term))

(defun free-3d-glterm (term)
  (let ((glarray (and term
                      (3d-glterm-contents-glarray term))))
    (when glarray (gl:free-gl-array glarray)))
  (setf (3d-glterm-contents-glarray term) nil))

(defmacro with-3d-glterm (var &body body)
  (let ((%var (gensym "term")))
    `(let (,%var)
       (unwind-protect
            (progn (setf ,var
                         (setf ,%var (make-3d-glterm '(5 5 5))))
                   ,@body)
         (free-3d-glterm ,%var)))))
