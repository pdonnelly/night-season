;;; Night Season, a stealth adventure game
;;; Copyright 2019 Paul Donnelly
;;;
;;; This file is part of Night Season.
;;;
;;; Night Season is free software: you can redistribute it and/or modify it
;;; under the terms of the GNU Affero General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.
;;;
;;; Night Season is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
;;; Affero General Public License for more details.
;;;
;;; You should have received a copy of the GNU Affero General Public
;;; License along with Night Season. If not, see
;;; <https://www.gnu.org/licenses/>.

(in-package :night-season)

(defun read-shader-source-file (file-name)
  (with-input-from-file (f file-name)
    (with-output-to-string (s)
      (loop
         for l = (read-line f nil)
         while l
         do (write-line l s)))))

(defparameter *max-char-textures* 128)

(defun upload-char (char)
  (let ((c (and (characterp char) (char-code char))))
    (assert (and c
                 (< c *max-char-textures*)))
    (gl:tex-sub-image-3d :texture-2d-array
                         0
                         0 0 c 8 16 1
                         :red
                         :unsigned-byte
                         (char-bitmap char))))

(defun char-bitmap (char)
  (cdr (assoc char *char-bitmaps*)))

(defstruct (glterm (:constructor %make-glterm))
  (width 80)
  (height 24)
  contents-glarray
  contents-buffer
  program)

(defun terminal-char-count (term)
  (* (glterm-width term) (glterm-height term)))

(defun terminal-set (term i char &optional (fg '(1 1 1)) (bg '(0 0 0)))
  (let ((a (glterm-contents-glarray term))
        (o (* 7 i)))
    (assert (< -1 o (- (gl:gl-array-byte-size a)
                       6)))
    (destructuring-bind (fr fg fb) fg
      (destructuring-bind (br bg bb) (or bg '(0 0 0))
        (setf (gl:glaref a o) (* 1.0 (char-code char))
              (gl:glaref a (+ o 1)) (* 1.0 fr)
              (gl:glaref a (+ o 2)) (* 1.0 fg)
              (gl:glaref a (+ o 3)) (* 1.0 fb)
              (gl:glaref a (+ o 4)) (* 1.0 br)
              (gl:glaref a (+ o 5)) (* 1.0 bg)
              (gl:glaref a (+ o 6)) (* 1.0 bb))))))

(defun terminal-clear (term)
  (iterate ((i (scan-range :below (terminal-char-count term))))
    (terminal-set term i #\space)))

(defun terminal-set-vertex-attribs (term)
  (gl:bind-buffer :array-buffer (glterm-contents-buffer term))
  (gl:vertex-attrib-pointer 1 1 :float nil 28 0) ; char index
  (gl:vertex-attrib-pointer 2 3 :float nil 28 4) ; fg color
  (gl:vertex-attrib-pointer 3 3 :float nil 28 16) ; bg color
  (%gl:vertex-attrib-divisor 1 1)
  (%gl:vertex-attrib-divisor 2 1)
  (%gl:vertex-attrib-divisor 3 1)
  (gl:enable-vertex-attrib-array 1)
  (gl:enable-vertex-attrib-array 2)
  (gl:enable-vertex-attrib-array 3))

(defun terminal-draw (term)
  (gl:use-program (glterm-program term))
  (%gl:named-buffer-sub-data
   (glterm-contents-buffer term)
   0
   (gl::gl-array-byte-size (glterm-contents-glarray term))
   (gl::gl-array-pointer (glterm-contents-glarray term)))
  (terminal-set-vertex-attribs term)
  (gl:draw-arrays-instanced :triangles 0 8 (terminal-char-count term)))

(defun make-glterm ()
  (let ((term (%make-glterm)))
    (let ((texture (gl:gen-texture)))
      (gl:bind-texture :texture-2d-array texture)
      (%gl:tex-storage-3d :texture-2d-array 1 :r8 8 16 *max-char-textures*)
      (gl:tex-parameter :texture-2d-array :texture-mag-filter :nearest))

    (collect-ignore (upload-char (car (scan *char-bitmaps*))))

    (let ((vshader (gl:create-shader :vertex-shader))
          (fshader (gl:create-shader :fragment-shader))
          (program (gl:create-program)))
      (gl:shader-source vshader (read-shader-source-file "sdl-ui/glterm.vert"))
      (gl:compile-shader vshader)
      (gl:shader-source fshader (read-shader-source-file "sdl-ui/glterm.frag"))
      (gl:compile-shader fshader)
      (let ((i (gl:get-shader-info-log fshader)))
        (when (plusp (length i)) (print i)))
      (let ((i (gl:get-shader-info-log vshader)))
        (when (plusp (length i)) (print i)))
      (gl:attach-shader program vshader)
      (gl:attach-shader program fshader)
      (gl:link-program program)
      (gl:delete-shader vshader)
      (gl:delete-shader fshader)
      (gl:use-program program)
      (gl:uniformi (gl:get-uniform-location program "char_textures") 0)
      (setf (glterm-program term) program))

    ;; Load a rectangle which will be textured as a character
    (let ((buffer (gl:gen-buffer)))
      (gl:bind-buffer :array-buffer buffer)
      (gl:vertex-attrib-pointer 0 2 :float nil 8 0)
      (gl:enable-vertex-attrib-array 0)

      (let* ((rect #(0.0 0.0  1.0 0.0  0.0 1.0
                     1.0 0.0  1.0 1.0  0.0 1.0))
             (array (gl:alloc-gl-array :float (length rect))))
        (collect-ignore
         (setf (gl:glaref array (scan-range :from 0))
               (scan rect)))
        (%gl:named-buffer-data buffer
                               (gl::gl-array-byte-size array)
                               (cffi:null-pointer)
                               :static-draw)
        (%gl:named-buffer-sub-data buffer
                                   0
                                   (gl::gl-array-byte-size array)
                                   (gl::gl-array-pointer array))
        (gl:free-gl-array array)))
    (let ((b (gl:gen-buffer)))
      (setf (glterm-contents-buffer term) b)
      (terminal-set-vertex-attribs term)
      (%gl::named-buffer-data b (* 80 24 7 4) (cffi:null-pointer) :stream-draw))
    (setf (glterm-contents-glarray term)
          (gl:alloc-gl-array :float (* (terminal-char-count term)
                                       (+ 1   ; char index
                                          3   ; fg color
                                          3   ; bg color
                                          ))))
    term))

(defun free-glterm (term)
  (let ((glarray (and term
                      (glterm-contents-glarray term))))
    (when glarray (gl:free-gl-array glarray)))
  (setf (glterm-contents-glarray term) nil))

(defmacro with-glterm (var &body body)
  (let ((%var (gensym "term")))
    `(let (,%var)
       (unwind-protect
            (progn (setf ,var
                         (setf ,%var (make-glterm)))
                   ,@body)
         (free-glterm ,%var)))))
