#version 330 core
layout (location = 0) in vec2 quad_vertex;
layout (location = 1) in float char;
layout (location = 2) in vec3 ifg_color;
layout (location = 3) in vec3 ibg_color;

out vec3 fg_color;
out vec3 bg_color;
out vec3 texcoord;

#define COLS 80;
#define ROWS 24;

void main()
{
    float h = 2.0/ROWS;
    float w = 2.0/COLS;
    int r = gl_InstanceID / COLS;
    int c = gl_InstanceID % COLS;
    vec2 spos = vec2(quad_vertex.x*w,-quad_vertex.y*h);
    vec2 off = vec2(-1.0,1.0)+vec2(c*w,-r*h);
    gl_Position = vec4(spos+off, 0.0f, 1.0f);
    fg_color = ifg_color;
    bg_color = ibg_color;
    texcoord = vec3(quad_vertex,char);
}
