;;; Night Season, a stealth adventure game
;;; Copyright 2019 Paul Donnelly
;;;
;;; This file is part of Night Season.
;;;
;;; Night Season is free software: you can redistribute it and/or modify it
;;; under the terms of the GNU Affero General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.
;;;
;;; Night Season is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
;;; Affero General Public License for more details.
;;;
;;; You should have received a copy of the GNU Affero General Public
;;; License along with Night Season. If not, see
;;; <https://www.gnu.org/licenses/>.

(in-package :night-season)

(defparameter *char-bitmaps*
  '((#\@ . #(0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 1 1 1 1 1 0 0
             1 1 0 0 0 1 1 0
             1 1 0 0 0 1 1 0
             1 1 0 1 1 1 1 0
             1 1 0 1 1 1 1 0
             1 1 0 1 1 1 0 0
             1 1 0 0 0 0 0 0
             0 1 1 1 1 1 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0))
    (#\. . #(0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 1 1 0 0 0
             0 0 0 1 1 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0))
    (#\, . #(0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 1 1 0 0 0
             0 0 0 1 1 0 0 0
             0 0 1 1 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0))
    (#\~ . #(0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 1 1 1 0 0 1 0
             1 1 1 1 1 1 1 0
             1 0 0 1 1 1 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0))
    (#\space . #(0 0 0 0 0 0 0 0
                 0 0 0 0 0 0 0 0
                 0 0 0 0 0 0 0 0
                 0 0 0 0 0 0 0 0
                 0 0 0 0 0 0 0 0
                 0 0 0 0 0 0 0 0
                 0 0 0 0 0 0 0 0
                 0 0 0 0 0 0 0 0
                 0 0 0 0 0 0 0 0
                 0 0 0 0 0 0 0 0
                 0 0 0 0 0 0 0 0
                 0 0 0 0 0 0 0 0
                 0 0 0 0 0 0 0 0
                 0 0 0 0 0 0 0 0
                 0 0 0 0 0 0 0 0
                 0 0 0 0 0 0 0 0))
    (#\% . #(0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 1 0
             0 1 1 0 0 1 1 0
             0 1 1 0 1 1 0 0
             0 0 0 1 1 0 0 0
             0 0 1 1 0 0 0 0
             0 1 1 0 0 0 0 0
             1 1 0 0 1 1 0 0
             1 0 0 0 1 1 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0))
    (#\# . #(0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 1 1 0 1 1 0 0
             0 1 1 0 1 1 0 0
             1 1 1 1 1 1 1 0
             0 1 1 0 1 1 0 0
             0 1 1 0 1 1 0 0
             1 1 1 1 1 1 1 0
             0 1 1 0 1 1 0 0
             0 1 1 0 1 1 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0))
    (#\> . #(0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 1 0 0 0 0 0
             0 0 1 1 0 0 0 0
             0 0 0 1 1 0 0 0
             0 0 0 0 1 1 0 0
             0 0 0 0 0 1 1 0
             0 0 0 0 0 1 0 0
             0 0 0 0 1 0 0 0
             0 0 0 1 0 0 0 0
             0 0 1 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0))
    (#\< . #(0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 1 0 0
             0 0 0 0 1 0 0 0
             0 0 0 1 0 0 0 0
             0 0 1 0 0 0 0 0
             0 1 0 0 0 0 0 0
             1 1 0 0 0 0 0 0
             0 1 1 0 0 0 0 0
             0 0 1 1 0 0 0 0
             0 0 0 1 1 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0))
    (#\) . #(0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 1 0 0 0 0 0
             0 0 1 1 0 0 0 0
             0 0 0 1 1 0 0 0
             0 0 0 0 1 1 0 0
             0 0 0 0 1 1 0 0
             0 0 0 0 1 1 0 0
             0 0 0 0 1 0 0 0
             0 0 0 1 0 0 0 0
             0 0 1 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0))
    (#\( . #(0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 1 0 0
             0 0 0 0 1 0 0 0
             0 0 0 1 0 0 0 0
             0 0 1 0 0 0 0 0
             0 1 1 0 0 0 0 0
             0 1 1 0 0 0 0 0
             0 1 1 0 0 0 0 0
             0 0 1 1 0 0 0 0
             0 0 0 1 1 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0))
    (#\a . #(0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 1 1 0 0 0
             0 1 1 0 1 1 0 0
             1 1 0 0 0 1 1 0
             1 1 0 1 0 1 1 0
             1 1 1 1 0 1 1 0
             1 1 0 0 0 1 1 0
             0 1 1 0 0 1 1 0
             0 0 0 0 0 0 1 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0))
    (#\b . #(0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 1 1 0 0 0 0 0
             0 1 1 0 0 0 0 0
             0 1 1 0 0 0 0 0
             0 1 1 0 1 0 0 0
             0 1 1 0 1 1 0 0
             0 1 1 0 0 1 1 0
             0 1 1 0 0 1 1 0
             0 1 1 0 0 1 1 0
             0 0 1 1 1 1 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0))
    (#\c . #(0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 1 1 1 1 1 0
             0 1 1 1 1 1 0 0
             1 1 0 0 0 0 0 0
             1 1 0 0 0 0 0 0
             1 1 1 0 0 0 0 0
             0 1 1 1 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0))
    (#\d . #(0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 1 1 1 0 0 0 0
             0 0 1 1 1 0 0 0
             0 0 0 1 1 1 0 0
             0 1 1 0 1 1 1 0
             1 1 0 0 0 1 1 0
             1 1 0 0 0 1 1 0
             1 1 0 0 0 1 1 0
             1 1 1 1 1 1 0 0
             0 1 1 1 1 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0))
    (#\e . #(0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 1 1 1 1 0 0 0
             1 1 0 1 1 1 0 0
             1 1 0 0 1 0 0 0
             1 1 0 1 0 0 0 0
             1 1 1 0 0 0 0 0
             0 1 1 1 1 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0))
    (#\f . #(0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 1 1 1 0 0 0
             0 1 1 1 1 1 0 0
             1 1 1 0 1 0 0 0
             0 1 1 0 0 0 0 0
             0 1 1 1 1 1 0 0
             0 1 1 1 1 0 0 0
             0 1 1 0 0 0 0 0
             0 1 1 0 0 0 0 0
             0 1 1 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0))
    (#\g . #(0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 1 1 1 1 0 0 0
             1 1 0 0 1 1 1 0
             1 1 0 0 1 1 1 0
             1 1 0 0 1 1 0 0
             0 1 1 1 1 1 0 0
             0 0 1 0 0 1 1 0
             0 1 0 0 0 1 1 0
             1 1 1 0 0 1 1 0
             0 0 1 1 1 1 0 0
             0 0 0 0 0 0 0 0))
    (#\h . #(0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             1 1 0 0 0 0 0 0
             1 1 0 0 0 0 0 0
             1 1 0 0 0 0 0 0
             1 1 0 0 1 1 0 0
             1 1 0 1 0 1 1 0
             1 1 0 0 0 1 1 0
             1 1 0 0 0 1 1 0
             1 1 0 0 0 1 1 0
             1 1 1 0 0 1 1 0
             0 0 0 0 0 1 0 0
             0 0 0 0 1 0 0 0
             0 0 0 1 0 0 0 0
             0 0 0 0 0 0 0 0))
    (#\i . #(0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 1 0 0 0 0
             0 0 1 1 1 0 0 0
             0 1 0 1 1 0 0 0
             0 0 0 1 1 0 0 0
             0 0 0 1 1 0 0 0
             0 0 0 1 1 1 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0))
    (#\j . #(0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 1 0 0 0 0
             0 0 1 1 1 0 0 0
             0 1 0 1 1 0 0 0
             0 0 0 1 1 0 0 0
             0 0 0 1 1 0 0 0
             0 0 0 1 1 0 0 0
             0 0 0 1 0 0 0 0
             0 0 1 0 0 0 0 0
             0 1 0 0 0 0 0 0
             0 0 0 0 0 0 0 0))
    (#\k . #(0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             1 1 0 0 0 0 0 0
             1 1 0 0 0 0 0 0
             1 1 0 0 0 0 0 0
             1 1 0 1 1 0 0 0
             1 1 0 0 1 1 0 0
             1 1 0 0 1 1 0 0
             1 1 0 1 1 0 0 0
             1 1 0 0 1 1 0 1
             1 1 1 0 0 1 1 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0))
    (#\l . #(0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 1 1 0 0 0 0 0
             0 0 1 1 0 0 0 0
             0 0 1 1 0 0 0 0
             0 0 1 1 0 0 0 0
             0 0 1 1 0 0 0 0
             0 0 1 1 0 0 0 0
             0 0 1 1 0 0 0 0
             0 0 1 1 0 0 0 0
             0 0 1 1 1 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0))
    (#\m . #(0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             1 0 0 1 0 0 1 0
             1 1 1 1 1 1 1 1
             1 1 0 1 1 0 1 1
             1 1 0 1 1 0 1 1
             1 1 0 1 1 0 1 1
             1 1 0 1 1 0 1 1
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0))
    (#\n . #(0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 1 0 0 0 1 0 0
             1 1 1 0 1 1 1 0
             0 1 1 1 0 1 1 0
             0 1 1 0 0 1 1 0
             0 1 1 0 0 1 1 0
             0 1 1 1 0 1 1 1
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0))
    (#\o . #(0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 1 1 1 1 0 0 0
             1 1 0 0 1 1 0 0
             1 1 0 0 1 1 0 0
             1 1 0 0 1 1 0 0
             1 1 0 0 1 1 0 0
             0 1 1 1 1 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0))
    (#\p . #(0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 1 0 0 1 1 0 0
             1 1 1 0 0 1 1 0
             0 1 1 0 0 1 1 0
             0 1 1 0 0 1 1 0
             0 1 1 1 1 1 1 0
             1 1 1 1 1 1 0 0
             0 1 1 0 0 0 0 0
             0 1 1 0 0 0 0 0
             0 1 1 0 0 0 0 0
             0 0 0 0 0 0 0 0))
    (#\q . #(0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 1 1 0 0 1 0 0
             1 1 0 0 1 1 0 0
             1 1 0 0 1 1 0 0
             1 1 0 0 1 1 0 0
             1 1 0 0 1 1 0 0
             0 1 1 1 1 1 1 0
             0 0 0 0 1 1 0 0
             0 0 0 0 1 1 0 0
             0 0 0 0 1 1 0 0
             0 0 0 0 0 0 0 0))
    (#\r . #(0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 1 0 0 0 1 0
             0 1 1 1 0 1 1 1
             1 0 1 1 1 0 1 0
             0 0 1 1 0 0 0 0
             0 0 1 1 0 0 0 0
             0 0 1 1 1 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0))
    (#\s . #(0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 1 1 1 0 0
             0 0 1 1 1 1 1 0
             0 1 1 1 0 1 0 0
             0 0 1 1 0 0 0 0
             0 0 1 1 0 0 0 0
             0 0 1 1 0 0 0 0
             0 0 1 1 0 0 0 0
             0 0 1 1 0 0 0 0
             0 0 1 1 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0))
    (#\t . #(0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 1 0 0 0 0
             0 1 1 1 1 1 1 0
             1 1 1 1 1 1 0 0
             0 0 1 1 0 0 0 0
             0 0 1 1 0 0 0 0
             0 0 1 1 1 0 0 0
             0 0 0 1 1 1 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0))
    (#\u . #(0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 1 0 0 0 1 0 0
             1 1 1 0 1 1 1 0
             0 1 1 0 0 1 1 0
             0 1 1 0 0 1 1 0
             0 1 1 0 1 1 1 0
             0 1 1 1 0 1 1 1
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0))
    (#\v . #(0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 1 0 0 0 1 0 0
             1 1 1 0 1 1 1 0
             0 1 1 0 0 1 1 0
             0 1 1 0 0 1 1 0
             0 1 1 0 0 1 1 0
             0 0 1 1 0 1 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0))
    (#\w . #(0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 1 1 0 0 0
             1 1 0 1 1 0 1 1
             1 1 0 1 1 0 1 1
             1 1 0 1 1 1 0 0
             1 1 0 1 1 0 1 1
             1 1 0 1 1 0 1 1
             1 1 1 1 1 1 0 0
             1 1 0 1 1 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0))
    (#\x . #(0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             1 1 0 0 1 1 0 0
             1 1 1 0 1 1 0 0
             0 1 1 1 0 0 0 0
             0 0 1 1 1 0 0 0
             1 1 0 1 1 1 0 0
             1 1 0 0 1 1 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0))
    (#\y . #(0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 1 0 0 0 1 0 0
             1 1 1 0 1 1 1 0
             0 1 1 0 0 1 1 0
             0 1 1 0 0 1 1 0
             0 1 1 0 1 1 1 0
             0 1 1 1 0 1 1 0
             0 0 0 0 0 1 1 0
             0 0 0 0 1 1 0 0
             0 0 1 1 0 0 0 0
             0 0 0 0 0 0 0 0))
    (#\z . #(0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 1
             1 1 1 1 1 1 1 0
             1 1 0 0 0 1 0 0
             0 0 0 0 1 0 0 0
             0 0 0 1 0 0 0 0
             0 0 1 0 0 0 1 1
             0 1 1 1 1 1 1 1
             1 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0))
    (#\_ . #(0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 1 1 1 1 1 1 0
             1 1 1 1 1 1 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0))
    (#\- . #(0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 1 1 1 1 1 0
             0 1 1 1 1 1 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0))
    (#\1 . #(0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 1 1 0 0 0
             0 0 1 1 1 0 0 0
             0 1 1 1 1 0 0 0
             0 0 0 1 1 0 0 0
             0 0 0 1 1 0 0 0
             0 0 0 1 1 0 0 0
             0 0 0 1 1 0 0 0
             0 1 1 1 1 1 1 0
             0 1 1 1 1 1 1 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0))
    (#\2 . #(0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 1 1 1 1 1 0 0
             1 1 1 1 1 1 1 0
             1 1 0 0 0 1 1 0
             0 0 0 0 0 1 0 0
             0 0 0 0 1 0 0 0
             0 0 0 1 0 0 0 0
             0 0 1 0 0 0 0 0
             0 1 1 1 1 1 1 0
             1 1 1 1 1 1 1 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0))
    (#\3 . #(0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             1 1 1 1 1 1 1 0
             1 1 1 1 1 1 0 0
             0 0 0 0 1 0 0 0
             0 0 0 1 1 1 0 0
             0 0 1 1 1 1 1 0
             0 0 0 0 0 1 1 0
             0 0 0 0 0 1 1 0
             1 1 1 1 1 1 0 0
             1 1 1 1 1 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0))
    (#\4 . #(0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 1 1 0 0
             0 1 1 0 1 1 0 0
             0 1 1 0 1 1 0 0
             0 1 0 0 1 1 0 0
             1 1 1 1 1 1 1 0
             1 1 1 1 1 1 1 0
             0 0 0 0 1 1 0 0
             0 0 0 0 1 1 0 0
             0 0 0 0 1 1 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0))
    (#\5 . #(0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 1 1 1 1 1 1 0
             1 1 1 1 1 1 0 0
             0 1 1 0 0 0 0 0
             0 1 1 1 1 1 0 0
             0 1 1 1 1 1 1 0
             0 0 0 0 0 1 1 0
             0 0 0 0 0 1 1 0
             0 1 1 1 1 1 1 0
             0 1 1 1 1 1 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0))
    (#\6 . #(0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 1 1 0 0
             0 0 0 1 1 0 0 0
             0 0 1 1 0 0 0 0
             0 1 1 1 1 0 0 0
             1 1 0 0 1 1 0 0
             1 1 0 0 0 1 1 0
             1 1 0 0 0 1 1 0
             1 1 1 1 1 1 1 0
             0 1 1 1 1 1 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0))
    (#\7 . #(0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             1 1 1 1 1 1 1 0
             1 1 1 1 1 1 1 0
             0 0 0 0 0 1 1 0
             0 0 0 0 1 1 0 0
             0 0 0 1 1 0 0 0
             0 0 0 1 1 0 0 0
             0 0 0 1 1 0 0 0
             0 0 0 1 1 0 0 0
             0 0 0 1 1 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0))
    (#\8 . #(0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 1 1 1 1 1 0 0
             1 1 1 1 1 1 1 0
             1 1 0 0 0 1 1 0
             1 1 1 1 1 1 1 0
             0 1 1 1 1 1 0 0
             1 1 0 0 0 1 1 0
             1 1 0 0 0 1 1 0
             1 1 1 1 1 1 1 0
             0 1 1 1 1 1 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0))
    (#\9 . #(0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 1 1 1 1 1 0 0
             1 1 1 1 1 1 1 0
             1 1 0 0 0 1 1 0
             0 1 1 0 0 1 1 0
             0 0 1 1 1 1 0 0
             0 0 0 1 1 0 0 0
             0 0 1 1 0 0 0 0
             0 1 1 0 0 0 0 0
             1 1 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0))
    (#\0 . #(0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 1 1 1 1 1 0 0
             1 1 1 1 1 1 1 0
             1 1 0 0 0 1 1 0
             1 1 0 0 0 1 1 0
             1 1 0 1 0 1 1 0
             1 1 0 0 0 1 1 0
             1 1 0 0 0 1 1 0
             1 1 1 1 1 1 1 0
             0 1 1 1 1 1 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0))
    (#\! . #(0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 1 1 0 0 0 0
             0 0 1 1 0 0 0 0
             0 0 1 1 0 0 0 0
             0 0 1 1 0 0 0 0
             0 0 0 1 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 1 1 0 0 0 0
             0 0 1 1 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0))
    (#\: . #(0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 1 1 0 0 0 0
             0 0 1 1 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 1 1 0 0 0 0
             0 0 1 1 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0))
    (#\; . #(0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 1 1 0 0 0 0
             0 0 1 1 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 1 1 0 0 0 0
             0 0 1 1 0 0 0 0
             0 1 1 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0))
    (#\' . #(0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 1 1 0 0 0
             0 0 0 1 1 0 0 0
             0 0 1 1 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0))
    (#\" . #(0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 1 1 0 0 1 1 0
             0 1 1 0 0 1 1 0
             1 1 0 0 1 1 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0))
    (#\^ . #(0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 1 0 0 0 0
             0 0 1 1 1 0 0 0
             0 1 1 0 1 1 0 0
             1 1 0 0 0 1 1 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0
             0 0 0 0 0 0 0 0))))
