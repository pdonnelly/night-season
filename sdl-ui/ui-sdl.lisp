;;; Night Season, a stealth adventure game
;;; Copyright 2019 Paul Donnelly
;;;
;;; This file is part of Night Season.
;;;
;;; Night Season is free software: you can redistribute it and/or modify it
;;; under the terms of the GNU Affero General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.
;;;
;;; Night Season is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
;;; Affero General Public License for more details.
;;;
;;; You should have received a copy of the GNU Affero General Public
;;; License along with Night Season. If not, see
;;; <https://www.gnu.org/licenses/>.

(in-package :night-season)

(defvar *current-glterm* nil)
(defvar *current-3d-glterm* nil)

(defun sdl2-keymod= (casekeymod inkeymod)
  (and (= (car inkeymod) (car casekeymod))
       (eql (second inkeymod)
            (reduce #'logior (cdr casekeymod)))))

(defmacro keyse (key mods &body cases)
  (let ((gkey (gensym "key"))
        (gmods (gensym "mods"))
        (glist (gensym "list")))
    `(let* ((,gkey ,key)
            (,gmods ,mods)
            (,glist (list ,gkey ,gmods)))
       (declare (ignorable ,gkey ,gmods ,glist))
       (cond ,@(mapcar
                (lambda (c)
                  (destructuring-bind (ckey . body) c
                    `(,(cond ((eq ckey t) t)
                             ((characterp ckey) `(= (char-code ,ckey) ,gkey))
                             ((atom ckey) `(eq ,ckey ,gkey))
                             ((listp ckey) `(sdl2-keymod= (list ,@ckey)
                                                          ,glist)))
                       ,@body)))
                cases)))))


(defmacro with-gc-to-log (ui message &body body)
  `(with-output-to-log (game (player ,ui))
     (let* ((*last-bytes-consed* (sb-ext:get-bytes-consed))
            (*last-gc-run-time* sb-ext:*gc-run-time*)
            (val (multiple-value-list (block nil ,@body))))
       (let* ((time sb-ext:*gc-run-time*)
              (cons (sb-ext:get-bytes-consed))
              (dtime (- time *last-gc-run-time*))
              (dcons (- cons *last-bytes-consed*)))
         (format t "~a" ,message)
         (and (plusp dtime) (format t "~ams GC. " dtime))
         (and (plusp dcons) (format t "~~~akB consed. " (round dcons 1024))))
       (values-list val))))


;; UI

(defvar *ui*)

(defstruct (ui (:constructor %make-ui))
  game
  window
  (%viewing-level :player) ; which z-level of the map the view is set at, xcom-style.
  (view-axis :z) ; which axis to view along
  (input "")
  (input-history '()))

(defun make-ui (window)
  (let* ((ui (%make-ui)))
    (setf (ui-window ui) window)
    ui))

(defun new-game (ui)
  (setf (ui-game ui) (make-stealth-game)))

(defmethod player ((u ui)) (player (ui-game u)))
(defmethod player-pos ((u ui)) (player-pos (ui-game u)))

(defun ui-viewing-level (ui)
  (if (numberp (ui-%viewing-level ui))
      (ui-%viewing-level ui)
      (pos-coordinate (pos (player ui))
                      (ui-view-axis ui))))
(defun viewing-level-follow-player (ui)
  (setf (ui-%viewing-level ui) :player))
(defun increment-viewing-level (ui n)
  (let ((new-level (+ n (ui-viewing-level ui))))
    (when (< -1
             new-level
             (first (zone-dimensions (pos-zone (pos (player (ui-game ui)))))))
      (setf (ui-%viewing-level ui) new-level))))


;; Drawing code
(defun draw-for-pos (pos axis char/color)
  (let ((row (if (eq axis :z) (pos-y pos) (pos-z pos)))
        (col (if (eq axis :x) (pos-y pos) (pos-x pos))))
    (when (and (< -1 row (glterm-height *current-glterm*))
               (< -1 col (glterm-width *current-glterm*)))
      (let ((c (typecase (car char/color)
                 (character char/color)
                 (number (cons (elt (format nil "~a" (car char/color)) 0)
                               (cdr char/color)))))
            (i (+ col (* row (glterm-width *current-glterm*)))))
        (when c
          (terminal-set *current-glterm* i
                        (char-downcase (car c))
                        (second c)
                        (third c)))))))

(defun draw-formatted (x y format-string color &rest args)
  (when (< -1 y (glterm-height *current-glterm*))
    (let ((s (apply #'format (list* nil format-string args))))
      (iterate ((x (+ x (scan-range :below (length s))))
                (c (scan t s)))
        (when (< -1 x (glterm-width *current-glterm*))
          (terminal-set *current-glterm* (+ x (* y 80)) (char-downcase c) color))))))


(defparameter *body-chars*
  '((player . #\@)
    (benny . #\b)
    (rob . #\R)
    (bob . #\B)
    (nob . #\N)
    (dob . #\D)
    (bert . #\b)
    (will . #\W)
    (bill . #\w)
    (geoff . #\G)
    (dick . #\d)
    (eddy . #\E)
    (hal . #\H)
    (tad . #\T)
    (laser . #\()
    (door . #\+)
    (staircase-top #\>)
    (staircase-middle #\%)
    (staircase-bottom #\<)
    (wall . #\%)
    (floor . #\;)
    (roof . #\^)
    (temp-earth . #\!)
    (unwalkable-earth . #\#)
    (surface-earth . #\.)
    (underground-earth . #\,)
    (water . #\~)
    (departure-zone . #\,)))

(defun find-view-pos (start-pos axis-key)
  (let ((p (scan-column start-pos axis-key t)))
    (or (collect-first (choose (mapping ((it p))
                                 (collect-first (memory-scan-bodies@ it)))
                               p))
        start-pos)))

(defun most-interesting (pos)
  (let ((m (memory-scan-bodies@ pos)))
    (collect-max (body-interest (tag m))
                 m)))

(defparameter *color-below* '(.8 .8 .8))
(defparameter *color-above* '(.8 .8 .8))
(defparameter *color-same* '(.8 .8 .8))
(defparameter *color-remembered* '(0 0 0))

(defun draw-remembered (ui vp)
  (when (memory-turns@ vp)
    (let* ((tag (cdr (assoc 'tag (most-interesting vp))))
           (lev (max 0
                     (- (pos-coordinate vp (ui-view-axis ui))
                        (pos-coordinate (player-pos ui) (ui-view-axis ui)))))
           (lev (if (and (eq :player tag)
                         (eq :z (ui-view-axis ui)))
                    (1+ lev)
                    lev))
           #- (and) (d (pos-coordinate (max-corner-pos vp) (ui-view-axis ui)))
           (bg (expt (- 1 (/ lev 5)) 2))
           (now (= (game-turns (game vp))
                   (memory-turns@ vp))))
      (draw-for-pos vp (ui-view-axis ui)
                    (list (cdr (assoc tag *body-chars*))
                          (if (not now)
                              *color-remembered*
                              (case 0
                                (-1 *color-above*)
                                (0 *color-same*)
                                (t *color-below*)))
                          (if (not now)
                              '(0.05 0 0)
                              (if (memory-litp@ vp)
                                  (list (* .3 bg) (* .3 bg) (* .1 bg))
                                  (list (* .05 bg) (* .05 bg) bg))))))))

(defun draw-fov (ui)
  (let* ((origin (player-pos ui))
         (axis (ui-view-axis ui)))
    (iterate ((p (scan-cube (make-pos (min-corner-pos origin)
                                      axis (ui-viewing-level ui))
                            (make-pos (max-corner-pos origin)
                                      axis (1+ (ui-viewing-level ui)))
                            t)))
      (let* ((vp (find-view-pos p axis))
             (view-depth (pos-coordinate p axis))
             (column (scan-column (make-pos p axis 0) axis t)))
        (draw-remembered ui vp)))))


(defun find-line-break (string end)
  (if (minusp end)
      nil
      (let ((end (min end (length string))))
        (collect-first
         (let* ((i (scan-range :from (1- end) :downto 0 :by -1)))
           (choose (char= #\space (aref string i))
                   i))))))

(defun calculate-line-breaks (string line-length)
  (let ((lack 0)
        (length 0))
    (values (collect
              (catenate (choose
                         (mapping ((i (scan-range :from line-length
                                                  :below (length string)
                                                  :by line-length)))
                           (let ((p (find-line-break string (- i lack))))
                             (when p
                               (incf lack (- i lack p))
                               (incf length))
                             p)))
                        (scan (list (length string)))))
            (1+ length))))

(defmacro scan-until (test &body body)
  "Repeatedly evaluate TEST until the result is true. Whenever the result is false, call BODY."
  (let ((dummy (gensym)))
    `(subseries
      (scan-fn t
               (lambda () nil)
               (lambda (,dummy) (declare (ignore ,dummy)) ,@body)
               (lambda (,dummy) (declare (ignore ,dummy)) ,test))
      1)))

(defun print-messages (x y messages line-length lines)
  (let ((lines-printed 0))
    (iterate ((m (until (scan-until (> lines-printed lines))
                        (scan 'list messages))))
      (multiple-value-bind (breaks lines)
          (calculate-line-breaks m line-length)
        (incf lines-printed lines)
        (collect-ignore
         (draw-formatted x (scan-range :from (- y lines-printed))
                         "~a" '(.7 .7 .7)
                         (subseq m
                                 (scan 'list (cons 0 breaks))
                                 (scan 'list breaks))))))))

(setf (language-string 'view-level '*english-strings*)
      '("View level " literal ", from " 'axis)
      (language-string 'player-pos '*english-strings*)
      '("Player at " literal)
      (language-string 'z '*english-strings*) "z"
      (language-string 'y '*english-strings*) "y"
      (language-string 'x '*english-strings*) "x")

(defun draw-ui-text (ui)
  (let ((game (ui-game ui)))
    (print-messages 30 15 (game-messages game) 50 16)
    (draw-formatted 1 15 "~a" '(1 1 1) (game-turns game))
    #- (and)
    (draw-formatted 1 16
                    "~a"
                    '(1 1 1)
                    (message1 nil `(view-level (literal . ,(ui-%viewing-level ui))
                                               (axis . ,(ui-view-axis ui)))))
    (draw-formatted 1 17 "~a"
                    '(1 1 1)
                    (message1 nil
                              `(player-pos (literal . ,(player-pos ui)))))))

(defun draw-ui (ui)
  (draw-ui-text ui)
  (let ((game (ui-game ui)))
    (if (player-wants-command-p game)
        (draw-formatted
         30 15
         "~a> ~a_" '(1 1 1) (game-turns game) (ui-input ui))
        (draw-formatted
         30 15
         "~a..." '(1 1 1) (game-turns game))))
  (draw-fov ui))

(defparameter *arrow-key-commands*
  '((:z . ((7 . (northwest nil))
           (4 . (west nil))
           (1 . (southwest nil))
           (2 . (south nil))
           (3 . (southeast nil))
           (6 . (east nil))
           (9 . (northeast nil))
           (8 . (north nil))))
    (:y . ((7 . (up-west nil))
           (4 . (west nil))
           (1 . (down-west nil))
           (2 . (down nil))
           (3 . (down-east nil))
           (6 . (east nil))
           (9 . (up-east nil))
           (8 . (up nil))))
    (:x . ((7 . (up-north nil))
           (4 . (north nil))
           (1 . (down-north nil))
           (2 . (down nil))
           (3 . (down-south nil))
           (6 . (south nil))
           (9 . (up-south nil))
           (8 . (up nil))))))

(defun arrow-key-command (ui number-on-key)
  (cdr
   (assoc number-on-key
          (cdr (assoc (ui-view-axis ui)
                      *arrow-key-commands*)))))

(define-command 'quit '((quit)))
(define-command 'view-z '((vz)))
(define-command 'view-y '((vy)))
(define-command 'view-x '((vx)))
(define-command 'new-game '((new-game) (new game)))
(define-command 'toggle-3d '((toggle-3d)))

(defmethod interpret-key-down (ui key mod unicode)
  (let ((start-time (get-internal-real-time)))
    (labels ((move (number-on-key)
               (submit-player-turn-parsed (player ui)
                                          (arrow-key-command ui number-on-key))))
      (keyse key mod
             ((sdl2-ffi:+sdlk-escape+ sdl2-ffi:+kmod-lctrl+) (sdl2:push-event :quit))
             (sdl2-ffi:+sdlk-kp-7+ (move 7))
             (sdl2-ffi:+sdlk-kp-4+ (move 4))
             (sdl2-ffi:+sdlk-kp-1+ (move 1))
             (sdl2-ffi:+sdlk-kp-2+ (move 2))
             (sdl2-ffi:+sdlk-kp-3+ (move 3))
             (sdl2-ffi:+sdlk-kp-6+ (move 6))
             (sdl2-ffi:+sdlk-kp-9+ (move 9))
             (sdl2-ffi:+sdlk-kp-8+ (move 8))
             (sdl2-ffi:+sdlk-kp-5+ (submit-player-turn-parsed (player ui) '(wait nil)))
             (sdl2-ffi:+sdlk-kp-plus+ (increment-viewing-level ui 1))
             (sdl2-ffi:+sdlk-kp-minus+ (increment-viewing-level ui -1))
             (sdl2-ffi:+sdlk-kp-multiply+ (viewing-level-follow-player ui))
             (sdl2-ffi:+sdlk-up+ (setf (ui-input ui) (pop (ui-input-history ui))))
             (sdl2-ffi:+sdlk-backspace+
              (with-slots ((i input)) ui
                (when (plusp (length i))
                  (setf i (subseq i 0 (1- (length i)))))))
             (sdl2-ffi:+sdlk-return+
              (with-slots ((i input)) ui
                (push i (ui-input-history ui))
                (let* ((player (player ui))
                       (c (parse *commands* player i)))
                  (with-output-to-log (game player)
                    (format t "~a. " i)
                    (if (stringp c)
                        (format t "~a" c)
                        (case (car c)
                          (toggle-3d (setf *draw-3d* (not *draw-3d*)))
                          (new-game (new-game ui))
                          (quit (sdl2:push-event :quit))
                          (view-z (setf (ui-view-axis ui) :z))
                          (view-y (setf (ui-view-axis ui) :y))
                          (view-x (setf (ui-view-axis ui) :x))
                          (look (look player))
                          (inventory (take-inventory player))
                          (directions (list-directions player))
                          (t (submit-player-turn-parsed (player ui) c))))))
                (setf i "")))
             (t (when-let ((c (and unicode (code-char unicode))))
                  (when (graphic-char-p c)
                    (setf (ui-input ui) (concatenate 'string (ui-input ui) (string c)))))))
      (with-output-to-log (game (player ui))
        (let* ((time sb-ext:*gc-run-time*)
               (cons (sb-ext:get-bytes-consed))
               (dtime (- time *last-gc-run-time*))
               (dcons (- cons *last-bytes-consed*))
               (drt (- (get-internal-real-time)
                       start-time)))
          (declare (ignorable dtime dcons drt))
          (setf *last-bytes-consed* (sb-ext:get-bytes-consed)
                *last-gc-run-time* sb-ext:*gc-run-time*)
          #- (and) (and (plusp dtime) (format t "~ams GC. " dtime))
          #- (and) (and (plusp dcons) (format t "~~~akB consed. " (round dcons 1024)))
          #- (and) (and (> drt 5) (format t "~ams. " drt)))))))



(defvar *draw-3d* nil)

(defun refresh-ui (ui)
  (gl:clear-color 0 0 0 1)
  (gl:clear :color-buffer)
  (terminal-clear *current-glterm*)
  (if (null (ui-game ui))
      (draw-formatted 5 3 "Preparing...." '(1 1 1))
      (draw-ui ui))
  (terminal-draw *current-glterm*)
  (sdl2:gl-swap-window (ui-window ui)))


;; Main

(defvar *last-bytes-consed*)
(defvar *last-gc-run-time*)

(defun run (&optional (io-stream nil io-stream-supplied-p)
              (width (* 80 8 2))
              (height (* 24 16 2)))
  (setf *last-bytes-consed* (sb-ext:get-bytes-consed)
        *last-gc-run-time* sb-ext:*gc-run-time*)
  (let ((*terminal-io* (if io-stream-supplied-p io-stream *terminal-io*)))
    (format *terminal-io* "Running Night Season...~%")
    (sdl2:with-init (:everything)
      (sdl2:with-window (w :w width :h height :flags '(:shown :opengl))
        (sdl2:with-gl-context (gl-context w)
          (sdl2:gl-make-current w gl-context)
          (gl:viewport 0 0 width height)
          (gl:enable :framebuffer-srgb)
          (gl:enable :blend)
          (gl:blend-func :src-alpha :dst-alpha)
          (gl:bind-vertex-array (gl:gen-vertex-array))

          (with-glterm *current-glterm*
            (with-3d-glterm *current-3d-glterm*
              (sdl2:start-text-input)
              (let ((ui (setf *ui* (make-ui w))))
                (refresh-ui ui)
                (new-game ui)
                (setf *last-turn-submission* (get-internal-real-time))
                (sdl2:with-event-loop (:method :poll)
                  (:idle ()
                         (refresh-ui ui)
                         (let ((now (get-internal-real-time)))
                           (when (and (< (* 0.1 internal-time-units-per-second)
                                         (- now *last-turn-submission*))
                                      (not (player-wants-command-p (ui-game ui))))
                             (submit-player-turn-parsed (player ui) nil))))
                  (:keydown (:keysym keysym)
                            (collect-ignore
                             (scan-until (player-wants-command-p (ui-game ui))
                               (submit-player-turn-parsed (player ui) nil)))
                            (let ((v (sdl2:sym-value keysym)))
                              (interpret-key-down ui
                                                  v
                                                  (sdl2:mod-value keysym)
                                                  (if (< -1 v 1114112)
                                                      v
                                                      nil))))
                  (:quit () t))))))))))

#+sbcl
(defun run-in-thread ()
  (sb-thread:make-thread #'run
                         :arguments (list *standard-output*)
                         :name "Night Season"))
