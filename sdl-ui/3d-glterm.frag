#version 330 core
in vec3 texcoord;
in vec3 fg_color;
in vec3 bg_color;

uniform sampler2DArray char_textures;

out vec4 FragColor;

void main()
{
    float c = (texcoord.z!=0)?texture(char_textures, texcoord).x:0.0;
    if(c==0) discard;
    FragColor = vec4(fg_color, 0.7f);
}
