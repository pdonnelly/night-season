(in-package #:night-season)

(defvar *english-strings* (make-hash-table :test #'equal))
(defvar *latin-strings* (make-hash-table :test #'equal))
(defvar *current-language-strings-symbol* '*english-strings*)
(defvar *default-language-strings-symbol* '*english-strings*)

(defun language-string (tag &optional (language-strings-symbol
                                       *current-language-strings-symbol*))
  (or (gethash tag (symbol-value language-strings-symbol))
      (gethash tag (symbol-value *default-language-strings-symbol*))))

(defsetf language-string (tag &optional (language-strings-symbol
                                         *current-language-strings-symbol*
                                         non-cur-p))
    (new-value)
  (if non-cur-p
      `(setf (gethash ,tag (symbol-value ,language-strings-symbol))
             ,new-value)
      `(setf (gethash ,tag (symbol-value *current-language-strings-symbol*))
             ,new-value)))

