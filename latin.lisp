;;; Night Season, a stealth adventure game
;;; Copyright 2019 Paul Donnelly
;;;
;;; This file is part of Night Season.
;;;
;;; Night Season is free software: you can redistribute it and/or modify it
;;; under the terms of the GNU Affero General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.
;;;
;;; Night Season is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
;;; Affero General Public License for more details.
;;;
;;; You should have received a copy of the GNU Affero General Public
;;; License along with Night Season. If not, see
;;; <https://www.gnu.org/licenses/>.

(in-package :night-season)

(iterate (((k v)
           (scan-alist
            '((cant-pick-self-up "Non potes se ipsum sumere. ")
              (cant-pick-itself-up actor " 9atur se ipsum sumere. ")
              (you-cant-reach "Id tangere non potes. ")
              (you-already-have "Id " object "iam habes. ")
              (you-got-the-object " sumis. ")
              (it-got-the actor object " sumit. ")
              (you-cant-drop-dont-have object " non habes. ")
              (you-drop object " ponis. ")
              (it-drops actor object " ponit. ")
              (in-your-way "Impediris " obstacle ", " direction ". ")
              (it-bumps-into actor " impeditur " obstacle ". ")
              (you-ascend "Ascendis. ")
              (you-descend "Descendis. ")
              (it-ascends actor " ascendit. ")
              (it-descends actor " descendit. ")
              (you-are-falling "Cadis. ")
              (it-is-falling actor " Cadit. ")
              (you-wait "You wait a moment. ")
              (no-way-to-ascend "There is no way to ascend here. ")
              (no-way-to-descend "There is no way to descend here. ")
              (something-blocking-stairs "There is something in the way. ")
              (inventory-intro "Tenes ")
              (holding-nothing "nihil. ")
              (look-intro "Apparentia: ")
              (things-just-above "Supra, ")
              (things-just-below "Infra, ")
              (possible-directions "You might go ")
              (no-possible-directions "There's no way you can go. ")
              (ambiguous-up-down "You might go either up or down. ")
              (between-turns "You are between turns. ")
              (understood-nothing "I'm afraid I understood nothing. ")
              (understood-up-to "I'm afraid I only understood as far as:")))))
  (setf (language-string k '*latin-strings*) v))
