;;; Night Season, a stealth adventure game
;;; Copyright 2019 Paul Donnelly
;;;
;;; This file is part of Night Season.
;;;
;;; Night Season is free software: you can redistribute it and/or modify it
;;; under the terms of the GNU Affero General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.
;;;
;;; Night Season is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
;;; Affero General Public License for more details.
;;;
;;; You should have received a copy of the GNU Affero General Public
;;; License along with Night Season. If not, see
;;; <https://www.gnu.org/licenses/>.

(in-package :night-season)

;; Maybe a way to get away from positions would be to put the slots on
;; the positioned objects. SCAN-ZONE can still use positions, but
;; because nothing would have a POS slot, storing a position would
;; demand copying it to the relevant slots. What about stuff like
;; FIND-VIEW-POS? It actually seems ok now... I'm not sure there are
;; any instances of storing a POS then accidentally continuing to
;; mutate it now. Makes it hard to think about how to prevent doing
;; that.

;; Positions
(defstruct (pos (:constructor %make-pos (zone &key x y z))
                (:print-function print-pos))
  "A position on a zone."
  zone (x 0 :type integer) (y 0 :type integer) (z 0 :type integer))

(defun pos-coordinate (pos axis-keyword)
  (funcall (ecase axis-keyword
             (:x #'pos-x)
             (:y #'pos-y)
             (:z #'pos-z))
           pos))

(defun print-pos (pos stream depth)
  (declare (ignore depth))
  (print-unreadable-object (pos stream :type t :identity nil)
    (format stream "~a ~a ~a" (pos-z pos) (pos-y pos) (pos-x pos))))

(defun make-pos (pos-or-zone &key x y z)
  (etypecase pos-or-zone
    (pos (remake-pos (shift pos-or-zone) :x x :y y :z z))
    ((or zone null) (%make-pos pos-or-zone
                                 :x (or x 0)
                                 :y (or y 0)
                                 :z (or z 0)))))

(defun remake-pos (pos &key x y z)
  "Replace the x, y, and z of a pos, returning the pos."
  (with-slots ((ox x) (oy y) (oz z)) pos
    (when x (setf ox x))
    (when y (setf oy y))
    (when z (setf oz z)))
  pos)

(defun pos= (pos1 pos2)
  "Do POS1 and POS2 indicate the exact same position on the same zone? True if both POS1 and POS2 are nil."
  (if (null pos1)
      (null pos2)
      (with-slots ((m1 zone) (x1 x) (y1 y) (z1 z)) pos1
        (with-slots ((m2 zone) (x2 x) (y2 y) (z2 z)) pos2
          (and (eq m1 m2)
               (= x1 x2)
               (= y1 y2)
               (= z1 z2))))))

(defgeneric shift (pos &key x y z)
  (:documentation "Add X, Y, and Z to the address of POS, returning a new pos.")
  (:method ((pos pos) &key (x 0) (y 0) (z 0))
    (with-slots (zone (ox x) (oy y) (oz z)) pos
      (make-pos zone :x (+ ox x) :y (+ oy y) :z (+ oz z)))))

(defun nshift (pos &key (x 0) (y 0) (z 0))
  "Destructively modify pos to change its address by X, Y, and Z."
  (with-slots ((ox x) (oy y) (oz z)) pos
    (incf ox x) (incf oy y) (incf oz z))
  pos)

(defun pos- (pos1 pos2)
  "The difference of the coordinates of POS1 and POS2. The zones need not match."
  (with-slots ((x1 x) (y1 y) (z1 z)) pos1
    (with-slots ((x2 x) (y2 y) (z2 z)) pos2
      (list (- z1 z2)
            (- y1 y2)
            (- x1 x2)))))

(defun clip-pos (pos)
  (with-slots (x y z zone) pos
    (destructuring-bind (d h w) (zone-dimensions zone)
      (make-pos zone
                :x (min (max 0 x) (1- w))
                :y (min (max 0 y) (1- h))
                :z (min (max 0 z) (1- d))))))

(defun nclip-pos (pos)
  (with-slots (x y z zone) pos
    (destructuring-bind (d h w) (zone-dimensions zone)
      (setf (pos-x pos) (min (max 0 x) (1- w))
            (pos-y pos) (min (max 0 y) (1- h))
            (pos-z pos) (min (max 0 z) (1- d)))))
  pos)

(defun dist (pos1 pos2)
  "The euclidean distance between POS1 and POS2. The zones need not match."
  (destructuring-bind (dz dy dx) (pos- pos2 pos1)
    (sqrt (+ (* dx dx) (* dy dy) (* dz dz)))))

;; Dungeon data structure
(defclass zone ()
  ((game :accessor zone-game :initarg :game) ; which game is this zone a part of?
   (dimensions :accessor zone-dimensions :initarg :dimensions)
   ;; arrays for map--"slices"
   (body :accessor zone-body)
   (memory :accessor zone-memory)
   (light :accessor zone-light)))

(defmethod initialize-instance :after ((zone zone)
                                       &key &allow-other-keys)
  (let ((d (zone-dimensions zone)))
    (setf (zone-body zone) (make-array d :initial-element nil)
          (zone-memory zone) (make-array d :initial-element nil)
          (zone-light zone) (make-array d :element-type
                                        'integer :initial-element 0))))

(defgeneric depart-zone (zone means)
  (:documentation "Depart from ZONE. MEANS will typically be a direction, such as 'north, which is used to decide on the destination."))

(defgeneric enter-zone (zone means)
  (:documentation "Enter ZONE. Must be called before attempting to start playing in ZONE. MEANS will typically be a direction, and is used to determine things like where in the zone the player begins."))

(defmethod shift ((zone zone) &key (x 0) (y 0) (z 0))
  (shift
   (if (and (<= 0 x) (<= 0 y) (<= 0 z))
       (min-corner-pos zone)
       (max-corner-pos zone))
   :z z :y y :x x))

(defun print-zone (zone stream depth)
  (declare (ignore depth))
  (print-unreadable-object (zone stream :type t :identity t)))

(defstruct (game (:constructor %make-game)
                 (:print-function print-game))
  "A GAME is the highest-level data structure of a game in progress and contains everything that would, for example, go in a save file."
  ;; Game is a struct because any specialization should happen on the
  ;; zone level. I.e. we attempt to make it possible to mix and match
  ;; content from different games.
  (player nil)             ; which body is the player
  (turns 0)                ; elapsed in-game time
  (messages '())           ; logged messages
  (saved-zones '())      ; any persistent zones (implentation TBD)
  )

(defun make-game ()
  (%make-game))

(defun print-game (game stream depth)
  (declare (ignore depth))
  (print-unreadable-object (game stream :type t :identity t)))

(defgeneric game (pos)
  (:method ((pos pos)) (zone-game (pos-zone pos))))

(defun pos-aref (array pos)
  (with-slots (z y x) pos
    (aref array z y x)))
(defun set-pos-aref (array pos new-value)
  (with-slots (z y x) pos
    (setf (aref array z y x) new-value)))
(defsetf pos-aref (array pos) (new-value)
  `(set-pos-aref ,array ,pos ,new-value))

(defgeneric min-corner-pos (pos)
  (:method ((pos (eql nil))) (error "POS must be non-null."))
  (:method ((pos pos))
    (make-pos (pos-zone pos)))
  (:method ((zone zone))
    (make-pos zone))
  (:method ((array array))
    (assert (= 3 (length (array-dimensions array))))
    (make-pos nil)))
(defgeneric max-corner-pos (pos)
  (:method ((pos (eql nil))) (error "POS must be non-null."))
  (:method ((pos pos))
    (destructuring-bind (z y x) (zone-dimensions (pos-zone pos))
      (make-pos (pos-zone pos) :z z :y y :x x)))
  (:method ((zone zone))
    (destructuring-bind (z y x) (zone-dimensions zone)
      (make-pos zone :z z :y y :x x)))
  (:method ((array array))
    (let ((dims (array-dimensions array)))
      (assert (= 3 (length dims)))
      (destructuring-bind (d h w) dims
        (make-pos nil :z d :y h :x w)))))

(defmacro define-layer-accessors (zone-accessor pos-accessor-symbol)
  `(progn
     (defun ,pos-accessor-symbol (pos)
       (pos-aref (,zone-accessor (pos-zone pos)) pos))
     (defsetf ,pos-accessor-symbol (pos) (new-value)
       (list 'setf (list 'pos-aref (list ',zone-accessor (list 'pos-zone pos)) pos) new-value))))

(define-layer-accessors zone-body %body@)
(define-layer-accessors zone-memory memory@)
(define-layer-accessors zone-light light@)



;; Iteration macros

(defun scan-cube-indices (z0 y0 x0 d h w) ; d=z, h=y, w=x
  (declare (optimizable-series-function 3))
  (let* ((dz (signum d))
         (dy (signum h))
         (dx (signum w))
         (d (abs d))
         (h (abs h))
         (w (abs w)))
    (map-fn '(values integer integer integer)
            (lambda (i)
              (values (+ z0 (* dz (truncate i (* h w))))
                      (+ y0 (* dy (mod (truncate i w) h)))
                      (+ x0 (* dx (mod i w)))))
            (scan-range :below (* d h w)))))

(defun scan-cube (pos1 pos2 &optional (re-use nil))
  (declare (optimizable-series-function))
  (let ((cur (shift pos1)))
    (destructuring-bind (d h w) (pos- pos2 pos1)
      (multiple-value-bind (z y x) (scan-cube-indices (pos-z pos1) (pos-y pos1) (pos-x pos1)
                                                      d h w)
        (if re-use
            (remake-pos cur :z z :y y :x x)
            (make-pos cur :z z :y y :x x))))))

(defun scan-column (p0 axis &optional (re-use nil))
  (declare (optimizable-series-function))
  (scan-cube p0
             (make-pos (shift p0 :z 1 :y 1 :x 1)
                       axis (pos-coordinate (max-corner-pos p0) axis))
             re-use))

(defun scan-zone (min-corner-or-zone &optional max-corner (re-use nil))
  "Scan a cube of positions in a zone. If MIN-CORNER-OR-ZONE is a pos, use it as the origin, otherwise use the origin of the zone. MAX-CORNER is optional and defaults to the max of the zone. RE-USE causes the scanned pos to be destructively modified."
  (declare (optimizable-series-function))
  (scan-cube (if (pos-p min-corner-or-zone)
                 min-corner-or-zone
                 (min-corner-pos min-corner-or-zone))
             (or max-corner (max-corner-pos min-corner-or-zone))
             re-use))

(defun scan-neighbors (pos &optional (re-use nil))
  (declare (optimizable-series-function))
  (scan-cube (nclip-pos (shift pos :z -1 :y -1 :x -1))
             (nclip-pos (shift pos :z 2 :y 2 :x 2))
             re-use))


(defun remove-body@ (pos body)
  (setf (%body@ pos) (delete body (%body@ pos) :test #'body=)))
(defun add-body@ (pos body)
  (when (consp body)
    (error "I thought we got rid of flyweights."))

  (setf (%body@ pos)
        ;; We allow a body to appear multiple times in a pos, cleaning
        ;; this up when we remove it from the pos, and this is visible
        ;; when we scan-body@. Maybe better to not? Also maybe better
        ;; to do some logic here to avoid the extra sequence unless
        ;; multiple bodies are actually present.
        (cons (if (consp body)
                  (car body)
                  body)
              (%body@ pos))))

(defun litp (pos) (plusp (light@ pos)))

(defgeneric player-pos (locale)
  (:method ((p (eql nil))) (declare (ignorable p)) nil)
  (:method ((g game)) (pos (game-player g)))
  (:method ((p pos)) (player-pos (game p))))

(defvar *inside-output-to-log* nil)
(defun call-with-log-output (game fn)
  (if *inside-output-to-log*
      (funcall fn)
      (let ((*inside-output-to-log* t)
            (o (with-output-to-string (*standard-output*)
                 (funcall fn))))
        (when (plusp (length o))
          (let ((turn (game-turns game)))
            (push (list turn (format nil "~a" o))
                  (game-messages game)))))))

(defmacro with-output-to-log (game &body body)
  `(call-with-log-output ,game (lambda () ,@body)))

(defun scan-body@ (pos)
  (declare (optimizable-series-function))
  (scan (%body@ pos)))

(defun find@ (body pos)
  (collect-first
   (choose-if (lambda (b) (body= b body))
              (scan-body@ pos))))

(defun game-zones (game)
  (union (list (pos-zone (pos (player game))))
         (game-saved-zones game)))

(defun map-all-bodies (fn game)
  "Call FN on every body in GAME. This means all in the current zone,
and all in any saved zones. Return NIL."
  (iterate ((z (scan (game-zones game))))
    (iterate ((p (scan-zone z nil t)))
      (iterate ((b (scan-body@ p)))
        (funcall fn b)))))

(defmacro do-all-bodies ((var game) &body body)
  `(map-all-bodies (lambda (,var) ,@body)
                   ,game))


(defgeneric player (locale)
  (:method ((g game)) (game-player g))
  (:method ((p pos)) (player (game p))))

(defun playerp (body)
  "Check if a given body is the player in its game."
  (eq body (player body)))

(defun human-game-turns (game)
  "Return the in-game time as (imprecise) human-readable turn count."
  (/ (game-turns game) 100.0))
