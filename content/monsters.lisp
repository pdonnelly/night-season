(in-package #:night-season)

(defclass player (mob fills-cell-mixin)
  ((ai :initform 'sim-player-ai)))

(define-body laser (emitting-light) ())
(define-body wall (scenery) ())
(define-body indoor-floor (scenery) ())
(define-body roof (scenery) ())

(define-body temp-earth () ())
(define-body unwalkable-earth (scenery) ())
(define-body surface-earth (scenery) ())
(define-body underground-earth (scenery) ())
(define-body water (scenery) (fills-cell opaque))
(define-body staircase-top (scenery) (fills-cell))
(define-body staircase-middle (scenery) (fills-cell))
(define-body staircase-bottom (scenery) (fills-cell))
(define-body bedrock (scenery) ())


(defun set-strings (tag citation-name name-words)
  (setf (language-string tag) citation-name
        (language-string (cons tag 'name-words)) name-words))

(set-strings 'player '(me myself) "you")
(set-strings 'laser '(laser gun) "laser gun")

(set-strings 'wall '(wall) "wall")
(set-strings 'indoor-floor '(floor) "floor")
(set-strings 'roof '(roof) "roof")

(set-strings 'temp-earth '(earth) "earth")
(set-strings 'unwalkable-earth '(earth) "earth")
(set-strings 'surface-earth '(earth) "earth")
(set-strings 'underground-earth '(earth) "earth")
(set-strings 'water '(water) "water")
(set-strings 'staircase-top '(stairs staircase) "stairs")
(set-strings 'staircase-middle '(stairs staircase) "stairs")
(set-strings 'staircase-bottom '(stairs staircase) "stairs")
(set-strings 'bedrock nil "bedrock")

(setf (language-string 'player '*latin-strings*) "tu")
