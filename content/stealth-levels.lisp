;;; Night Season, a stealth adventure game
;;; Copyright 2019 Paul Donnelly
;;;
;;; This file is part of Night Season.
;;;
;;; Night Season is free software: you can redistribute it and/or modify it
;;; under the terms of the GNU Affero General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.
;;;
;;; Night Season is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
;;; Affero General Public License for more details.
;;;
;;; You should have received a copy of the GNU Affero General Public
;;; License along with Night Season. If not, see
;;; <https://www.gnu.org/licenses/>.

(in-package :night-season)

(defun choose-lacking (tag series)
  (declare (optimizable-series-function)
           (off-line-port series))
  (choose-if (lambda (p) (not (find@ tag p)))
             series))

(defclass zone-basic-town (zone) ()
  (:documentation "This zone is the basic town street, akin to the basic dungeon in your basic roguelike."))

(defmethod depart-zone ((z zone-basic-town) means)
  (enter-zone (make-instance 'zone-basic-town
                             :game (zone-game z)
                             :dimensions '(10 15 30))
              :test))

(defun half-fill-zone (zone)
  (iterate ((p (scan-zone
                (shift zone
                       :z (truncate (first (zone-dimensions zone))
                                    2))
                nil t)))
    (setf (pos (make-instance 'surface-earth)) p)))

(defun two-volume-zone (zone)
  (iterate ((p (scan-zone zone nil t)))
    (setf (pos (make-instance 'surface-earth)) p))
  (iterate ((p (scan-zone (shift zone :z 5 :y 5)
                          (shift zone :z 8 :y 8 :x 10))))
    (let ((b (filler@ p)))
      (setf (pos b) nil)))
  (iterate ((p (scan-zone (shift zone :z 5 :y 4 :x 10)
                          (shift zone :z 8 :y 9 :x 20))))
    (setf (pos (filler@ p)) nil)))

(defun roads-island-zone (zone)
  (half-fill-zone zone)
  (let ((ground-level (truncate (first (zone-dimensions zone))
                                2)))
    ))

(defmethod initialize-instance :after ((zone zone-basic-town)
                                       &key &allow-other-keys)
  (let* ((fs #(roads-island-zone))
         (f (elt fs (random (length fs)))))
    (funcall f zone))
  zone)

(defmethod enter-zone ((zone zone-basic-town) means)
  (declare (ignore means))
  (let* ((pos (collect-first
               (choose-if (complement #'filler@)
                          (scan-zone zone nil t)))))
    (unless pos (error "Made a map w/o a single walkable tile."))
    (setf (pos (player (zone-game zone))) pos)
    (setf (pos (make-instance 'laser)) (shift pos :y 1))))

(defun make-stealth-game ()
  (let* ((game (make-game))
         (player (make-instance 'player)))
    (setf (game-player game) player)
    (enter-zone (make-instance 'zone-basic-town
                               :game game
                               :dimensions '(10 15 30))
                'test)
    (begin-turn game)
    game))
