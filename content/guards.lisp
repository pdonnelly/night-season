(in-package #:night-season)

(defparameter *guards*
  '(benny rob bob nob dob bert will bill geoff dick eddy hal tad ted todd)
  "In the experiement of having only uniques, here are some guards. They are listed here so level gen knows where to find guards.")

(dolist (g *guards*)
  (setf (language-string g) (list g 'guard)
        (language-string (cons g 'name-words)) (string-downcase (symbol-name g) :start 1)))

(defclass guard (mob)
  ((ai :initform (make-guard-ai))))

;; awareness
(defclass awareness-sensor (ai-sm)
  ((state :initform 'unaware)
   (awareness :initform 0 :accessor awareness)
   (last-seen :initform nil :accessor last-seen)))

(defun %update-awareness (ai)
  (let* ((p (pos *self*))
         (player (player p))
         (v (visibility player))
         (visible (and (in-player-fov-p p) (plusp v)))
         (delta-vis (if visible v -1))
         (new-awareness (min 30
                             (max 0 (+ delta-vis (awareness ai))))))
    (setf (awareness ai) new-awareness)
    (when visible (setf (last-seen ai) (pos player)))
    (values)))

(defmethod do-ai-for-state ((ai awareness-sensor) (state (eql 'unaware)))
  (%update-awareness ai)
  (unless (< (awareness ai) 20)
    (senses became-aware)
    (setf (ai-state ai) 'aware)))

(defmethod do-ai-for-state ((ai awareness-sensor) (state (eql 'aware)))
  (%update-awareness ai)
  (if (> (awareness ai) 10)
      (senses aware (last-seen (last-seen ai)))
      (progn (senses became-unaware)
             (setf (ai-state ai) 'unaware))))

;; strategy
(defclass guard-strategic-decider (ai-sm)
  ((state :initform 'guarding)))

(defun %begin-pursuit (ai)
  (senses pursue)
  (setf (ai-state ai) 'pursuit))

(defmethod do-ai-for-state ((ai guard-strategic-decider) (state (eql 'guarding)))
  (cond ((sense became-aware) (%begin-pursuit ai))
        (nil ;;(assigned-patrol-points *self*)
         (senses patrol)
         (setf (ai-state ai) 'patrolling))))

(defmethod do-ai-for-state ((ai guard-strategic-decider) (state (eql 'patrolling)))
  (if (sense became-aware)
      (%begin-pursuit ai)
      (senses patrol)))

(defmethod do-ai-for-state ((ai guard-strategic-decider) (state (eql 'pursuit)))
  (if (sense became-unaware)
      (setf (ai-state ai) 'guarding)
      (senses pursue)))

;; pursuit
(defun path-or-continue (ai dest max-iterations)
  (unless (current-path ai)
    (multiple-value-bind (path cs)
        (if (pathing-state ai)
            (pathfind *self* nil nil max-iterations (pathing-state ai))
            (pathfind *self* (pos *self*) dest max-iterations))
      (setf (current-path ai) path
            (pathing-state ai) cs)))
  (prog1 (path-first-command (current-path ai))
    (pop (current-path ai))))

(defclass pursuit-executor (ai-sm)
  ((state :initform 'inactive)
   (destination :initform nil :accessor destination)
   (pursuit-ok :initform t :accessor pursuit-ok-p)
   (current-path :initform nil :accessor current-path)
   (pathing-state :initform nil :accessor pathing-state)))

(defmethod do-ai-for-state ((ai pursuit-executor) (state (eql 'inactive)))
  (when (sense pursue) (setf (ai-state ai) 'active
                             (pursuit-ok-p ai) t)))

(setf (language-string 'guard-bark-cant-see '*english-strings*)
      '(name ": \"Can't see...\" "))

(defmethod do-ai-for-state ((ai pursuit-executor) (state (eql 'active)))
  (if (not (sense pursue))
      (setf (ai-state ai) 'inactive)
      (let* ((sighting (sense last-seen))
             (cmd (path-or-continue ai sighting 500)))
        (command cmd)
        (when (and (null cmd) (pursuit-ok-p ai))
          (message1 t `(guard-bark-cant-see (name . ,(tag *self*))))
          (setf (pursuit-ok-p ai) nil)))))

;; patrol
(defclass patrol-executor (ai-sm)
  ((state :initform 'inactive)
   (patrol-points :initform nil :initarg :patrol-points :accessor patrol-points)
   (current-path :initform nil :accessor current-path)
   (pathing-state :initform nil :accessor pathing-state)))

(defmethod do-ai-for-state ((ai patrol-executor) (state (eql 'inactive)))
  (when (sense patrol) (setf (ai-state ai) 'active)))

(defun can-see-p (body pos) (pos= pos (pos body)))

(defmethod do-ai-for-state ((ai patrol-executor) (state (eql 'active)))
  (if (not (sense patrol))
      (setf (ai-state ai) 'inactive)
      (progn
        (when (null (patrol-points ai))
          (setf (patrol-points ai) (mapcar (lambda (p) (cons p 0))
                                           nil
                                           ;;(assigned-patrol-points *self*)
                                           )))
        (setf (patrol-points ai)
              (collect (mapping (((p time) (scan-alist (patrol-points ai))))
                                (cons p (if (can-see-p *self* p)
                                            (game-turns (game p))
                                            time)))))
        (when-let* ((dest (car (first (sort (patrol-points ai) #'< :key #'cdr))))
                    (cmd (path-or-continue ai dest 200)))
          (command cmd)))))

;; bark
(defclass bark-executor (ai-sm)
  ((state :initform 'unaware)))

(setf (language-string 'guard-bark-thief '*english-strings*)
      '(name ": \"Thief!\" "))

(defmethod do-ai-for-state ((ai bark-executor) (state (eql 'unaware)))
  (when (sense became-aware)
    (setf (ai-state ai) 'aware)
    (message1 t `(guard-bark-thief (name . ,(tag *self*))))))

(setf (language-string 'guard-bark-rats  '*english-strings*)
      '(name ": \"Must have been rats.\" ")
      (language-string 'guard-bark-still-aware  '*english-strings*)
      '(name ": \"Urgh I'm so aware!\" "))

(defmethod do-ai-for-state ((ai bark-executor) (state (eql 'aware)))
  (if (sense became-unaware)
      (progn (setf (ai-state ai) 'unaware)
             (message1 t `(guard-bark-rats (name . ,(tag *self*)))))
      (when (zerop (random 10))
        (message1 t `(guard-bark-still-aware (name . ,(tag *self*)))))))

(defun random-stumble-executor ()
  (command (if (sense stumble)
               (list (whichever 'north 'northeast 'east
                                'southeast 'south 'southwest
                                'west 'northwest 'wait)                     
                     nil)
               '(wait nil))))

;; rest
(defun default-to-resting-executor ()
  (unless (sense command)
    (command '(wait nil))))

(defun make-guard-ai ()
  (list (make-instance 'awareness-sensor)
        (make-instance 'guard-strategic-decider)
        (make-instance 'pursuit-executor)
        (make-instance 'patrol-executor)
        (make-instance 'bark-executor)
        'default-to-resting-executor))
