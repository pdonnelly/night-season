;;; Night Season, a stealth adventure game
;;; Copyright 2019 Paul Donnelly
;;;
;;; This file is part of Night Season.
;;;
;;; Night Season is free software: you can redistribute it and/or modify it
;;; under the terms of the GNU Affero General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.
;;;
;;; Night Season is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
;;; Affero General Public License for more details.
;;;
;;; You should have received a copy of the GNU Affero General Public
;;; License along with Night Season. If not, see
;;; <https://www.gnu.org/licenses/>.

(in-package :night-season)


;; Level generation

(defun map-flatbottom-triangle (x1 y1 x2 y2 x3 y3 fn)
  (let ((islope1 (/ (- x2 x1)
                    (- y2 y1)
                    1.0))
        (islope2 (/ (- x3 x1)
                    (- y3 y1)
                    1.0)))
    (loop
       for y from y1 upto y2
       for left = x1 then (+ left islope1)
       for right = x1 then (+ right islope2)
       do (map-horizontal-line y (floor left) (floor right) fn))))

(defun map-flattop-triangle (x1 y1 x2 y2 x3 y3 fn)
  (let ((islope1 (/ (- x3 x1)
                    (- y3 y1)))
        (islope2 (/ (- x3 x2)
                    (- y3 y2))))
    (loop
       for y from (1+ y1) upto y3
       for left = x1 then (+ left islope1)
       for right = x2 then (+ right islope2)
       do (map-horizontal-line y (floor left) (floor right) fn))))

(defun map-horizontal-line (y x1 x2 fn)
  (iterate ((x (scan-range :from (min x1 x2) :below (max x1 x2))))
    (funcall fn x y)))

(defun map-triangle (xtop ytop xmid ymid xbot ybot fn)
  "Call FN (of X and Y) on each integer coordinate inside a triangle.
The TOP, MID, and BOT arguments correspond to three vertices, sorted
so that the Ys are non-decreasing from first to last argument."
  (cond ((= ymid ybot) (map-flatbottom-triangle xtop ytop xmid ymid xbot ybot fn))
        ((= ytop ymid) (map-flattop-triangle xtop ytop xmid ymid xbot ybot fn))
        (t
         (let ((splitx (+ xtop
                          (* (/ (- ymid ytop)
                                (- ybot ytop))
                             (- xbot xtop)))))
           (map-flatbottom-triangle xtop ytop xmid ymid splitx ymid fn)
           (map-flattop-triangle xmid ymid splitx ymid xbot ybot fn)))))

