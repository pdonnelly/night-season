;;; Night Season, a stealth adventure game
;;; Copyright 2019 Paul Donnelly
;;;
;;; This file is part of Night Season.
;;;
;;; Night Season is free software: you can redistribute it and/or modify it
;;; under the terms of the GNU Affero General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.
;;;
;;; Night Season is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
;;; Affero General Public License for more details.
;;;
;;; You should have received a copy of the GNU Affero General Public
;;; License along with Night Season. If not, see
;;; <https://www.gnu.org/licenses/>.

(in-package :night-season)

 ;;; Helpers
(defun lit-from-viewpoint-p (pos viewpoint)
  (or (litp pos)
      ;; Treat the floor or ceiling as lit if the adjacent cell is lit
      (if (not (blocks-fov-p pos))
          nil
          (let ((pz (pos-z viewpoint))
                (cz (pos-z pos)))
            (cond ((> pz cz) (litp (shift pos :z 1))))
            (cond ((< pz cz) (litp (shift pos :z -1))))))))

(defun body-digest (body)
  (list (cons 'tag (tag body))))

(defun memorize-fov (body)
  (let* ((fov (calculate-fov body))
         (body-pos (pos body))
         (turns (game-turns (game body-pos))))
    (setf (time-of-most-recent-fov body) turns)
    (iterate ((p (choose-if (lambda (it) (plusp (pos-aref fov it)))
                            (scan-zone (min-corner-pos body-pos) nil t))))
      (setf (memory@ p)
            (list* turns
                   (lit-from-viewpoint-p p (pos body))
                   (collect (body-digest (scan-body@ p))))))))

(defun memory-turns@ (pos) (first (memory@ pos)))
(defun memory-litp@ (pos) (second (memory@ pos)))
(defun memory-scan-bodies@ (pos)
  (declare (optimizable-series-function))
  (scan 'list (cddr (memory@ pos))))
(defun find-memory@ (tag pos)
  (collect-first
   (choose-if (lambda (b) (eq tag (cdr (assoc 'tag b))))
              (memory-scan-bodies@ pos))))

(defun in-player-fov-p (pos)
  (= (or (time-of-most-recent-fov (player pos)) -2)
     (or (car (memory@ pos)) -1)))

 ;;; AI "engine"
(defvar *senses* nil
  "Stores senses in play during execution of an AI. This is how AIs
  earlier in a list AI communicate to those later.")

(defvar *self* nil
  "During execution of an AI, bound to the body in question.")

(defclass ai-sm ()
  ((state :accessor ai-state
          :initform (error "This state class should have supplied an initform."))))

(defun push-senses (senses)
  (setf *senses* (append senses *senses*))
  (values))

(defmethod do-ai ((ai ai-sm))
  "Evaluate an AI."
  (do-ai-for-state ai (ai-state ai)))

(defmethod do-ai ((ai cons))
  "A list is also an AI, which is run by running each item in turn."
  (dolist (a ai) (do-ai a)))

(defmethod do-ai ((ai function))
  "A function is an AI with no state."
  (funcall ai))

(defmethod do-ai ((ai symbol))
  (funcall ai))

(defmacro senses (&rest senses)
  "A helper to make it nicer to return senses. A bare symbol FOO
means (foo . t) as a sense, while a (BAR (BAZ)) means (BAR . (BAZ))
where BAZ is evaluated. NIL leaves no trace in the output."
  `(push-senses
    (list ,@(collect
              (choose
               (mapping ((s (scan senses)))
                 (etypecase s
                   (null)
                   (symbol `'(,s . t))
                   (cons `(cons ',(first s) ,(second s))))))))))

(defmacro command (command &rest senses)
  "A helper macro to make it nicer to issue commands. COMMAND produces
a :command sense, and the following are interpreted as in SENSES. If
COMMAND is NIL, it leaves no trace."
  (if command
      `(senses (command ,command) ,@senses)
      `(senses ,@senses)))

(defmacro sense (sense)
  "A helper to make it nicer to check for senses."
  `(cdr (assoc ',sense *senses*)))

(defun ai-decide (body)
  "Run the AI for BODY. If a command is in the sensory data at the end of the run, return that command, otherwise re-run the AI, giving it any 'feedback in the returned sensory data."
  (let ((*self* body)
        (*senses* nil)
        (ai (actor-ai body))
        (start-time (get-internal-real-time)))
    (loop
      (do-ai ai)
      (let* ((now (get-internal-real-time))
             (run-time (- now start-time)))
        (when (>= run-time (* 0.5 internal-time-units-per-second))
          (cerror "Think more?" "~A has been thinking for ~ams." body run-time)
          (setf start-time (get-internal-real-time))))
      (let* ((command (sense command)))
        (when command (return-from ai-decide command))
        (setf *senses*
              (remove 'feedback *senses* :test-not #'eq :key #'car))))))

 ;;; Pathfinding
(defun path-first-step (path)
  (if (null path)
      nil
      (let* ((current (first path))
             (next (second path)))
        (cond ((not current) nil)    ; path couldn't be found so no step
              ((not next) nil)       ; we're at the destination so no step
              (t (pos- current next))))))

(defun path-first-command (path)
  (delta-direction (path-first-step path)))

(defun delta-direction (zyx-list)
  "Given a list of x, y, z coordinates, see if it matches a direction
  command."
  `(move-zyx (,zyx-list)))
