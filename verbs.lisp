;;; Night Season, a stealth adventure game
;;; Copyright 2019 Paul Donnelly
;;;
;;; This file is part of Night Season.
;;;
;;; Night Season is free software: you can redistribute it and/or modify it
;;; under the terms of the GNU Affero General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.
;;;
;;; Night Season is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
;;; Affero General Public License for more details.
;;;
;;; You should have received a copy of the GNU Affero General Public
;;; License along with Night Season. If not, see
;;; <https://www.gnu.org/licenses/>.

(in-package :night-season)

(defun message1 (destination message)
  (destructuring-bind (m &rest params) (ensure-list message)
    (let ((s
           (collect-append
            'string
            (mapping ((s (scan (ensure-list (language-string m)))))
              (etypecase s
                (string (format destination "~a" s))
                ((eql literal)
                 (format destination "~a" (cdr (assoc s params))))
                (symbol
                 (format destination "~a" (language-string (cdr (assoc s params))))))))))
      (and (null destination) s))))

(defun message (destination actor player-message &optional (npc-message '(nil nil)))
  "Print PLAYER-MESSAGE if ACTOR is the player, otherwise optional
NPC-MESSAGE if supplied, to DESTINATION, according to the current
language. Returns the message as a string if DESTINATION is nil,
otherwise returns nil."
  (message1 destination (if (playerp actor) player-message npc-message)))

(defstruct (verb (:constructor make-verb (name verify-fns check-fn do-fn is-ui-verb)))
  "grammars: A list of grammars. verify-fns: A list of functions, one for each verb argument, which rank candidate objects for logicality. check-fn: A function taking an argument for each verb argument, which decides whether the action is possible. do-fn: A function taking an argument for each verb argument, which carries out the effects of the action. is-ui-verb: If this verb is a UI verb, then it should not end the player's turn in a turn-based game."
  name
  verify-fns
  check-fn
  do-fn
  is-ui-verb)

(defvar *verb-failed-p* nil)
(defun fail-verb ()
  (setf *verb-failed-p* t))

(defun check-command (actor command args)
  (let ((verb (command-verb command)))
    (when (null verb)
      (cerror "Continue as if verb had failed."
              "The command ~a is defined but the verb is not. "
              command)
      (return-from check-command nil))
    (let* ((*verb-failed-p* nil)
           (cfn (verb-check-fn verb)))
      (and cfn (apply cfn actor args))
      (not *verb-failed-p*))))

(defun exec-command (actor command args)
  (let ((verb (command-verb command)))
    (when (null verb)
      (cerror "Continue as if verb had failed."
              "The command ~a is defined but the verb is not. "
              command)
      (return-from exec-command nil))
    (let* ((*verb-failed-p* nil)
           (cfn (verb-check-fn verb))
           (result (and cfn (apply cfn actor args))))
      (if *verb-failed-p*
          nil   ; Commands which don't pass check don't end your turn.
          ;; Your turn ends unless it was a UI verb.
          (prog1 (not (verb-is-ui-verb verb))
            (apply (verb-do-fn verb)
                   actor
                   (concatenate 'list args result)))))))

;;; Get command
(define-situation check-get nil (actor noun))
(define-situation do-get nil (actor noun))

(iterate (((k v)
           (scan-alist
            '((cant-pick-self-up "You can't pick yourself up. ")
              (cant-pick-itself-up actor " tries to pick itself up. ")
              (you-cant-reach "You can't reach that. ")
              (you-already-have "You already have the " object ". ")
              (you-got-the "You got the " object ". ")
              (it-got-the actor " picks up " object ". ")
              (you-cant-drop-dont-have "You hold no " object ". ")
              (you-drop "You drop the " object ". ")
              (it-drops actor " drops the " object ". ")
              (in-your-way "In the way: " obstacle ", " direction ". ")
              (it-bumps-into actor " bumps into " obstacle ". ")
              (you-ascend "You ascend. ")
              (you-descend "You descend. ")
              (it-ascends actor " ascends. ")
              (it-descends actor " descends. ")
              (you-fall "You land. ")
              (it-falls actor " lands. ")
              (you-are-falling "You fall. ")
              (it-is-falling actor " falls. ")
              (you-wait "You wait a moment. ")
              (no-way-to-ascend "There is no way to ascend here. ")
              (no-way-to-descend "There is no way to descend here. ")
              (something-blocking-stairs "There is something in the way. ")
              (inventory-intro "You are holding ")
              (holding-nothing "nothing. ")
              (look-intro "Things visible: ")
              (things-just-above "Above you, ")
              (things-just-below "Below you, ")
              (possible-directions "You might go ")
              (no-possible-directions "There's no way you can go. ")
              (ambiguous-up-down "You might go either up or down. ")
              (between-turns "You are between turns. ")
              (understood-nothing "I'm afraid I understood nothing. ")
              (understood-up-to "I'm afraid I only understood as far as:")
              (departure-zone-test "You are in a departure zone.")
              ((1 0 -1) . "down and west")
              ((1 0 0) . "straight down")
              ((1 0 1) . "down and east")
              ((1 1 -1) . "down and southwest")
              ((1 1 0) . "down and south")
              ((1 1 1) . "down and southeast")
              ((1 -1 -1) . "down and northwest")
              ((1 -1 0) . "down and north")
              ((1 -1 1) . "down and northeast")
              ((0 0 -1) . "down and west")
              ((0 0 0) . "in the same place")
              ((0 0 1) . "east")
              ((0 1 -1) . "southwest")
              ((0 1 0) . "south")
              ((0 1 1) . "southeast")
              ((0 -1 -1) . "northwest")
              ((0 -1 0) . "north")
              ((0 -1 1) . "northeast")
              ((-1 0 -1) . "up and west")
              ((-1 0 0) . "straight up")
              ((-1 0 1) . "up and east")
              ((-1 1 -1) . "up and southwest")
              ((-1 1 0) . "up and south")
              ((-1 1 1) . "up and southeast")
              ((-1 -1 -1) . "up and northwest")
              ((-1 -1 0) . "up and north")
              ((-1 -1 1) . "up and northeast")))))
  (setf (language-string k '*english-strings*) v))

(define-rule check-get cant-get-self (actor noun)
  (when (eq actor noun)
    (fail-verb)
    (message t actor
             'cant-pick-self-up
             `(cant-pick-itself-up (actor . ,(tag actor))))))
(define-rule check-get item-present (actor noun)
  (when (null (find@ noun (pos actor)))
    (fail-verb)
    (message t actor 'you-cant-reach)))
(define-rule check-get item-not-held (actor noun)
  (when (member noun (body-inventory actor))
    (fail-verb)
    (message t actor `(you-already-have (object . ,(tag noun))))))

(define-rule do-get base-rule (actor noun)
  (setf (pos noun) actor)
  (message t actor
           `(you-got-the (object . ,(tag noun)))
           `(it-got-the (actor . ,(tag actor))
                        (object . ,(tag noun)))))

;; Drop command
(define-situation check-drop nil (actor arg))
(define-situation do-drop nil (actor arg))

(define-rule check-drop item-held (actor noun)
  (unless (member noun (body-inventory actor))
    (fail-verb)
    (message t actor
             `(you-cant-drop-dont-have (object . ,(tag noun))))))

(define-rule do-drop base-rule (actor arg)
  (setf (pos arg) (pos actor))
  
  (message t actor
           `(you-drop (object . ,(tag arg)))
           `(it-drops
             (actor . ,(tag actor))
             (object . ,(tag arg)))))

(defun take-inventory (actor)
  (message t actor 'inventory-intro)
  (if-let ((it (body-inventory actor)))
    (let ((names (mapcar #'citation-name it)))
      (format t ": ~a~{, ~a~}. " (first names) (rest names)))
    (message t actor 'holding-nothing)))
(defun things-in-fov (body)
  (let ((all-visible '())
        (fov (body-fov body)))
    (iterate ((p (scan-zone (pos-zone (pos body)) nil t)))
      (when (plusp (pos-aref fov p))
        (setf all-visible (append (collect (scan-body@ p)) all-visible))))
    all-visible))

(defun look (actor)
  (let ((names (collect
                 (choose
                  (scan
                   (mapcar #'citation-name
                           (remove-if (lambda (thing)
                                        (< (body-interest thing) 50))
                                      (things-in-fov actor))))))))
    (message t actor 'look-intro)
    (format t "~a~{, ~a~}. " (first names) (rest names)))
  (take-inventory actor)
  (when-let (it (collect
                  (citation-name
                   (cdr (assoc 'tag (memory-scan-bodies@ (shift actor :z 1)))))))
    (message t actor 'things-just-below)
    (format t "~a~{, ~a~}. " (first it) (rest it)))
  (when-let (it (collect
                  (tag (cdr (assoc 'tag (memory-scan-bodies@ (shift actor :z -1)))))))
    (message t actor 'things-just-above)
    (format t "~a~{, ~a~}. " (first it) (rest it))))

(define-situation check-move nil (actor z y x))
(define-situation do-move nil (body z y x))

(defun direction-description (dxyz)
  (destructuring-bind (dz . dxy) dxyz
    (concatenate 'string
                 (case dz
                   (-1 "above and ")
                   (0 "")
                   (1 "below and "))
                 (cdr (assoc dxy
                             '(((0 -1)   . "west")
                               ((0 0)    . "in the same place")
                               ((0 1)    . "east")
                               ((1 -1)   . "southwest")
                               ((1 0)    . "south")
                               ((1 1)    . "southeast")
                               ((-1 -1)  . "northwest")
                               ((-1 0)   . "north")
                               ((-1 1)   . "northeast"))
                             :test #'equal)))))

(defun list-directions (actor)
  (let* ((n (scan-neighbors actor t))
         (ds (collect
               (direction-description
                (pos- (pos actor)
                      (choose (and (can-occupy-p actor n)
                                   (not (can-occupy-p actor (shift n :z 1))))
                              n))))))
    (if (null ds)
        (message t actor 'no-possible-directions)
        (progn (message t actor 'possible-directions)
               (format t "~a~{, ~a~}. " (car ds) (cdr ds))))))

(defun dest-for-move (body z y x)
  "Suppose we're walking with the given (x,y) delta, x,y∈{-1,0,1}.
  Sometimes there is a block in that destination spot, and this
  implies climbing on top of it or going below it. When unambiguous,
  return that pos above or below it. Otherwise, return the destination
  which would otherwise be walked to, even if blocked. Return a second
  value indicating whether it was ambiguous."
  (if (not (zerop z))
      (shift body :z z :y y :x x)
      (let* ((dest (shift body :y y :x x))
             (above-dest (shift dest :z -1))
             (below-dest (shift dest :z 1)))
        (cond ((can-occupy-p body dest) (values dest nil)) 
              ((can-occupy-p body above-dest)
               (if (can-occupy-p body below-dest)
                   (values dest t)
                   (values above-dest nil)))
              ((can-occupy-p body below-dest) (values below-dest nil))
              (t (values dest nil))))))

;; To support automatic up/down diagonal moves, the thing to do is
;; have check-move reject moves unless the dest is unambiguous.
;; Technically it ought to be the job of a verify function to
;; determine the existence of multiple destinations and to pick the
;; correct destination when unambiguous.
(define-rule check-move base-rule (body z y x)
  (multiple-value-bind (dest ambiguous) (dest-for-move body z y x)
    (let ((filler (filler@ dest))
          (src (pos body)))
      (cond (ambiguous
             (fail-verb)
             (message t body 'ambiguous-up-down))
            (filler
             (fail-verb)
             (message t body
                      `(in-your-way
                        (obstacle . ,(tag filler))
                        (direction . ,(mapcar #'signum (pos- src dest))))
                      `(it-bumps-into
                        (actor . ,(tag body))
                        (obstacle . ,(tag filler)))))))))

(define-rule do-move base-rule (body z y x)
  (let* ((p (pos body))
         (dest (dest-for-move body z y x)))
    (setf (pos body) dest)
    (case (signum (- (z dest) (z p)))
      (-1 (message t body 'you-ascend
                   `(it-ascends (actor . ,(tag body)))))
      (0 nil)
      (1 (message t body 'you-descend
                  `(it-descends (actor . ,(tag body))))))))

(define-rule do-move (base-rule departure-zone ()) (body z y x)
  (when (departure-zone-p (pos body))
    (message t body 'departure-zone-test)))

(define-situation do-wait nil (body))
(define-rule do-wait base-rule (body) (message t body 'you-wait))

(define-situation check-take-stairs nil (body dir))
(define-situation do-take-stairs nil (body dir))
(define-rule check-take-stairs base-rule (body dir)
  (assert (or (= dir -1) (= dir 1)))
  (let ((dest (shift body :z dir)))
    (cond ((not (climbablep body dest))
           (fail-verb)
           (message t body (if (= dir -1)
                               'no-way-to-ascend
                               'no-way-to-descend)))
          ((filler@ dest)
           (fail-verb)
           (message t body 'something-blocking-stairs)))))
(define-rule do-take-stairs base-rule (body dir)
  (let ((dest (shift body :z dir)))
    (setf (pos body) dest)
    (message t body (if (= dir -1)
                        'you-ascend
                        'you-descend))))

(eval-when (:compile-toplevel :load-toplevel :execute)
  (defvar *verbs* (make-hash-table))
  (defvar *commands* (make-hash-table)))

(defun command-verb (command &optional (verbs *verbs*))
  (gethash command verbs))

(defun add-verb (new-verb &optional (verbs *verbs*))
  (setf (gethash (verb-name new-verb) verbs) new-verb))

(defun define-verb (name verify-fns check-fn do-fn)
  (add-verb (make-verb name verify-fns check-fn do-fn nil)))

(defun define-ui-verb (name verify-fns check-fn do-fn)
  "A UI verb is a verb which does not end the player's turn, like viewing inventory or setting game options."
  (add-verb (make-verb name verify-fns check-fn do-fn t)))

(defun define-command (name grammars)
  (setf (gethash name *commands*) (make-command name grammars)))

(defun things-in-scope (actor)
  (mapcar (lambda (thing) (cons (name-words thing) thing))
          (append (things-in-fov actor)
                  (body-inventory actor))))



(define-command 'look '((look) (l)))
(define-command 'inventory '((inventory) (i)))
(define-command 'directions '((directions) (dirs)))
(define-command 'get `((get ,#'things-in-scope) (g ,#'things-in-scope)
                       (pick up ,#'things-in-scope) (pick ,#'things-in-scope up)
                       (take ,#'things-in-scope)))
(define-command 'drop `((drop ,#'things-in-scope) (d ,#'things-in-scope)
                        (put down ,#'things-in-scope) (put ,#'things-in-scope down)
                        (let ,#'things-in-scope go) (let ,#'things-in-scope fall)))
(define-command 'wait '((wait) (z)))

(define-command 'north '((north) (n)))
(define-command 'northeast '((northeast) (ne)))
(define-command 'east '((east) (e)))
(define-command 'southeast '((southeast) (se)))
(define-command 'south '((south) (s)))
(define-command 'southwest '((southwest) (sw)))
(define-command 'west '((west) (w)))
(define-command 'northwest '((northwest) (nw)))

(define-command 'up-north '((up north) (un)))
(define-command 'up-northeast '((up northeast) (une)))
(define-command 'up-east '((up east) (ue)))
(define-command 'up-southeast '((up southeast) (use)))
(define-command 'up-south '((up south) (us)))
(define-command 'up-southwest '((up southwest) (usw)))
(define-command 'up-west '((up west) (uw)))
(define-command 'up-northwest '((up northwest) (unw)))

(define-command 'down-north '((down north) (dn)))
(define-command 'down-northeast '((down northeast) (dne)))
(define-command 'down-east '((down east) (de)))
(define-command 'down-southeast '((down southeast) (dse)))
(define-command 'down-south '((down south) (ds)))
(define-command 'down-southwest '((down southwest) (dsw)))
(define-command 'down-west '((down west) (dw)))
(define-command 'down-northwest '((down northwest) (dnw)))

(define-command 'up '((up) (u) (<)))
(define-command 'down '((down) (d) (>)))

(define-command 'path '((path :nnn :nnn :nnn) (p :nnn :nnn :nnn)))
(define-command 'teleport '((teleport :nnn :nnn :nnn) (tele :nnn :nnn :nnn)))

(define-command 'depart '((depart)))


(define-verb 'get '() #'check-get #'do-get)
(define-verb 'drop () #'check-drop #'do-drop)
(define-verb 'wait '() nil #'do-wait)

(define-verb 'move-zyx '()
  (lambda (actor zyx)
    (if (null zyx)
        (fail-verb)
        (apply #'check-move actor zyx)))
  (lambda (actor zyx)
    (apply #'do-move actor zyx)))

(defun define-dir-verb (name z y x)
  (define-verb name '() 
    (lambda (actor) (check-move actor z y x))
    (lambda (actor) (do-move actor z y x))))
(define-dir-verb 'north 0 -1 0)
(define-dir-verb 'northeast 0 -1 1)
(define-dir-verb 'east 0 0 1)
(define-dir-verb 'southeast 0 1 1)
(define-dir-verb 'south 0 1 0)
(define-dir-verb 'southwest 0 1 -1)
(define-dir-verb 'west 0 0 -1)
(define-dir-verb 'northwest 0 -1 -1)

(define-dir-verb 'up-north -1 -1 0)
(define-dir-verb 'up-northeast -1 -1 1)
(define-dir-verb 'up-east -1 0 1)
(define-dir-verb 'up-southeast -1 1 1)
(define-dir-verb 'up-south -1 1 0)
(define-dir-verb 'up-southwest -1 1 -1)
(define-dir-verb 'up-west -1 0 -1)
(define-dir-verb 'up-northwest -1 -1 -1)

(define-dir-verb 'down-north 1 -1 0)
(define-dir-verb 'down-northeast 1 -1 1)
(define-dir-verb 'down-east 1 0 1)
(define-dir-verb 'down-southeast 1 1 1)
(define-dir-verb 'down-south 1 1 0)
(define-dir-verb 'down-southwest 1 1 -1)
(define-dir-verb 'down-west 1 0 -1)
(define-dir-verb 'down-northwest 1 -1 -1)

(define-verb 'up
    '()
  (lambda (actor) (check-take-stairs actor -1))
  (lambda (actor) (do-take-stairs actor -1)))
(define-verb 'down
    '()
  (lambda (actor) (check-take-stairs actor 1))
  (lambda (actor) (do-take-stairs actor 1)))

(define-verb 'path
  nil nil
  (lambda (actor z y x)
    (let* ((p (pos actor))
           (path (pathfind actor p
                           (make-pos p :z z :y y :x x)
                           200000))
           (c (path-first-command path)))
      (if (null path)
          (format t "no path from ~a. " p)
          (apply #'exec-command actor c)))))

(define-verb 'depart
    '() nil
  (lambda (actor)
    (depart-zone (pos-zone (pos actor)) 'test)))
