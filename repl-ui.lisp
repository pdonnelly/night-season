;;; Night Season, a stealth adventure game
;;; Copyright 2019 Paul Donnelly
;;;
;;; This file is part of Night Season.
;;;
;;; Night Season is free software: you can redistribute it and/or modify it
;;; under the terms of the GNU Affero General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.
;;;
;;; Night Season is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
;;; Affero General Public License for more details.
;;;
;;; You should have received a copy of the GNU Affero General Public
;;; License along with Night Season. If not, see
;;; <https://www.gnu.org/licenses/>.

;;; Some functions to make the game somewhat playable from the REPL.

(in-package :night-season)

(defun most-interesting (pos)
  (let ((m (memory-scan-bodies@ pos)))
    (collect-max (body-interest (cdr (assoc 'tag m)))
                 m)))

(defparameter *body-chars*
  '((player . #\@)
    (benny . #\b)
    (rob . #\R)
    (bob . #\B)
    (nob . #\N)
    (dob . #\D)
    (bert . #\b)
    (will . #\W)
    (bill . #\w)
    (geoff . #\G)
    (dick . #\d)
    (eddy . #\E)
    (hal . #\H)
    (tad . #\T)
    (laser . #\()
    (door . #\+)
    (staircase-top #\>)
    (staircase-middle #\%)
    (staircase-bottom #\<)
    (wall . #\%)
    (indoor-floor . #\;)
    (roof . #\^)
    (temp-earth . #\!)
    (unwalkable-earth . #\#)
    (surface-earth . #\.)
    (underground-earth . #\,)
    (water . #\~)
    (departure-zone . #\,)))

(defun remembered-char (vp)
  (if (not (memory-turns@ vp))
      #\space
      (let* ((tag (cdr (assoc 'tag
                              (most-interesting vp)))))
        (or (cdr (assoc tag *body-chars*))
            #\space))))

(defun find-view-pos (start-pos axis-key)
  (let ((p (scan-column start-pos axis-key t)))
    (or (collect-first
         (choose
          (mapping ((it p))
            (collect-first
             (memory-scan-bodies@ it)))
          p))
        start-pos)))

(defun fov-array (game axis viewing-level)
  (let* ((origin (player-pos game))
         (viewing-level
           (or viewing-level
               (pos-coordinate origin
                               axis)))
         (min (make-pos (min-corner-pos origin)
                        axis viewing-level))
         (max
           (make-pos (max-corner-pos origin)
                     axis (1+ viewing-level)))
         (dims (pos- max min))
         (a (make-array (ecase axis
                          (:z (cdr dims))
                          (:y (list (first dims)
                                    (third dims)))
                          (:x (butlast dims)))
                        :initial-element #\%)))
    (iterate ((p (scan-cube min max t)))
      (setf (aref
             a
             (if (eq axis :z) (pos-y p) (pos-z p))
             (if (eq axis :x) (pos-y p) (pos-x p)))
            (remembered-char
             (find-view-pos p axis))))
    a))

(defun parse-submit (game string)
  (let ((c (parse *commands* (player game) string)))
    (if (stringp c)
        c
        (submit-player-turn-parsed (player game) c))))

(defun print-fov (game &optional (axis :z) viewing-level)
  (let* ((a (fov-array game axis viewing-level))
         (w (array-dimension a 1)))
    (iterate ((i (scan-range :below
                             (array-total-size a)))
              (c (scan 'array a)))
      (when (= (1- w) (mod i w))
        (terpri))
      (write-char c)))
  (values))

(defun scan-until-fn (test-fn step-fn)
  "Repeatedly evaluate TEST until the result is true. Whenever the result is false, call BODY."
  (declare (optimizable-series-function))
  (subseries
   (scan-fn t
            (lambda () nil)
            (lambda (dummy)
              (declare (ignore dummy))
              (funcall step-fn))
            (lambda (dummy)
              (declare (ignore dummy))
              (funcall test-fn)))
   1))

(defmacro scan-until (test &body body)
  `(scan-until-fn
    (lambda () ,test)
    (lambda () ,@body)))

(defun find-line-break (string end)
  (if (minusp end)
      nil
      (let ((end (min end (length string))))
        (collect-first
         (let* ((i (scan-range :from (1- end) :downto 0 :by -1)))
           (choose (char= #\space (aref string i))
                   i))))))

(defun calculate-line-breaks (string line-length)
  (let ((lack 0)
        (length 0))
    (values
     (collect
       (catenate
        (choose
         (mapping ((i (scan-range
                       :from line-length
                       :below (length string)
                       :by line-length)))
                  (let ((p (find-line-break
                            string
                            (- i lack))))
                    (when p
                      (incf lack (- i lack p))
                      (incf length))
                    p)))
        (scan (list (length string)))))
     (1+ length))))

(defun messages-print (messages line-length lines)
  (let ((lines-printed 0))
    (iterate ((m (until
                  (scan-until-fn (lambda ()
                                   (> lines-printed
                                      lines))
                                 (lambda ()))
                  (scan messages))))
      (multiple-value-bind (breaks lines)
          (calculate-line-breaks m line-length)
        (incf lines-printed lines)
        (collect-ignore
         (format t "~a~%"
                 (subseq m
                         (scan (cons 0 breaks))
                         (scan breaks)))))))
  (values))

#- (and) (setf g (make-stealth-game))
