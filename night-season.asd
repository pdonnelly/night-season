;;; Night Season, a stealth adventure game
;;; Copyright 2019 Paul Donnelly
;;;
;;; This file is part of Night Season.
;;;
;;; Night Season is free software: you can redistribute it and/or modify it
;;; under the terms of the GNU Affero General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.
;;;
;;; Night Season is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
;;; Affero General Public License for more details.
;;;
;;; You should have received a copy of the GNU Affero General Public
;;; License along with Night Season. If not, see
;;; <https://www.gnu.org/licenses/>.

(asdf:defsystem #:night-season
  :serial t
  :description "An early-modern stealth adventure game"
  :author "Paul Donnelly"
  :depends-on (#:sdl2 #:cl-opengl
                      #:series #:alexandria)
  :components ((:file "package")
               (:file "set-up-series")
               (:file "situations")
               (:file "language")
               (:file "game-map")
               (:file "bodies")
               (:file "levelgen")
               (:file "pathfinding")
               (:file "fov")
               (:file "ai")
               (:file "parse")
               (:file "verbs")
               (:file "advance-world")
               (:file "repl-ui")
               (:file "term")
               (:module "sdl-ui"
                :serial t
                :components ((:file "lagamon-bitmaps")
                             (:file "glterm")
                             (:file "3d-term")
                             (:file "ui-sdl")))
               (:module "content"
                :serial t
                :components ((:file "stealth-levels")
                             (:file "guards")
                             (:file "monsters")))))

