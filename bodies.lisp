;;; Night Season, a stealth adventure game
;;; Copyright 2019 Paul Donnelly
;;;
;;; This file is part of Night Season.
;;;
;;; Night Season is free software: you can redistribute it and/or modify it
;;; under the terms of the GNU Affero General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.
;;;
;;; Night Season is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
;;; Affero General Public License for more details.
;;;
;;; You should have received a copy of the GNU Affero General Public
;;; License along with Night Season. If not, see
;;; <https://www.gnu.org/licenses/>.

(in-package :night-season)

;; Monsters and items

(defclass positioned ()
  ((pos :reader pos :initform nil :initarg :pos))
  (:documentation "Mixin for all classes of object which go on the map, whether physical or more like metadata."))

(defclass departure-zone (positioned) ())

(defclass base-body (positioned)
  ((emitting-light :accessor emitting-light-p :initform nil)
   (opaque :accessor opaquep :initform t)
   (fills-cell :accessor fills-cell-p :initform t)
   (scenery :accessor sceneryp :initform nil))
  (:documentation "Base class shared by all bodies having a physical existence."))

(defun tag (body) (class-name (class-of body)))

(defmacro define-body (class-name t-properties nil-properties)
  `(defclass ,class-name (base-body)
     ,(append
       (collect (list (scan t-properties) :initform t))
       (collect (list (scan nil-properties) :initform nil)))))

(defclass probabilistic-speed-mixin ()
  ((acting-this-turn :accessor acting-this-turn-p :initform nil))
  (:documentation "This mixin has the slots for the probabilistic speed system. The intent is to keep them separate to make it easier to remove if the system doesn't work out."))

(defclass actor-mixin ()
  ((chosen-action :accessor chosen-action :initform nil)
   (ai :accessor actor-ai :initform nil :initarg :ai)
   (fov :accessor fov :initform nil)
   (time-of-most-recent-fov :accessor time-of-most-recent-fov :initform nil)
   (inventory :accessor inventory :initform nil)
   (visibility :accessor visibility :initform 0)))

(defclass mob (probabilistic-speed-mixin actor-mixin base-body)
  ())

(defgeneric citation-name (body)
  (:method (body) (language-string (tag body)))
  (:method ((body symbol)) (language-string body)))

(defgeneric name-words (body)
  (:method (body)
    (language-string (cons (tag body) 'name-words)))
  (:method ((body symbol))
    (language-string (cons body 'name-words)))  )

(defun print-body (body stream depth)
  (declare (ignore depth))
  (print-unreadable-object (body stream :type t :identity t)
    #- (and) (format stream "~a" (tag body))))

(defmethod shift ((body positioned) &key (x 0) (y 0) (z 0))
  (shift (pos body) :x x :y y :z z))

(defgeneric %do-removal-steps (body old-pos)
  (:method ((body base-body) (old-pos pos))
    (when (opaquep body)
      (assert (minusp (light@ old-pos))
              nil
              "Light level is ~a when removing ~a."
              (light@ old-pos) body)
      (incf (light@ old-pos)))
    (when (emitting-light-p body)
      (iterate ((p (choose-if #'litp (scan-neighbors old-pos t))))
        (decf (light@ p))
        (assert (<= 0 (light@ p))))))
  (:method :after ((body actor-mixin) (old-pos pos))
    (dolist (i (inventory body))
      (%do-removal-steps i old-pos))))

(defgeneric %do-placement-steps (body new-pos)  
  (:method ((body base-body) (new-pos pos))
    (when (opaquep body)
      (if (minusp (light@ new-pos))
          (decf (light@ new-pos))
          (setf (light@ new-pos) -1)))
    (when (emitting-light-p body)
      (iterate ((p (choose-if (lambda (p) (<= 0 (light@ p)))
                              (scan-neighbors new-pos t))))
        (incf (light@ p)))))
  (:method :after ((body actor-mixin) (new-pos pos))
    (dolist (i (inventory body))
      (%do-placement-steps i new-pos))))

(defun body= (a b)
  (eq (if (consp a) (car a) a)
      (if (consp b) (car b) b)))

(defsetf pos (body) (new-pos)
  `(%reposition-body ,body ,new-pos))

(defun %reposition-body (body new-pos)
  (let ((new-pos (shift new-pos)))
    (when-let* ((_ (fills-cell-p body))
                (it (and new-pos (filler@ new-pos))))
      (error "Tried to position a filler (~a) where another filler (~a) already is (~a)."
             body it new-pos))

    ;; Steps to remove from map/parent
    (let ((old-pos (pos body)))
      (when old-pos
        (if (pos-p old-pos)
            (progn
              (%do-removal-steps body old-pos)            
              (remove-body@ old-pos body))
            (progn ;; assume old-pos is a body's inventory then
              (%do-removal-steps body (pos old-pos))
              (setf (inventory old-pos)
                    (remove body (inventory old-pos) :test #'body=))))))

    ;; Update the body itself.
    ;; Note that reposition-body should not call setf pos on body -- will
    ;; just recurse.
    (setf (slot-value body 'pos) new-pos)

    ;; Steps to add to map
    (when new-pos
      (if (pos-p new-pos)
          (progn (add-body@ new-pos body)
                 (%do-placement-steps body new-pos))
          (progn ;; must be adding to an actor
            (push body (inventory new-pos))
            (%do-placement-steps body (pos new-pos))))))
  body)

(defun body-interest (tag)
  "Compute how interesting a body is to the player, with 100 being pretty interesting. Useful for deciding what item to show on top of a stack and so forth, or what to mention when looking."
  (cond ((member tag *guards*) 100)
        ((getf '(player 100
                 laser 90
                 door 80
                 wall 40
                 staircase-top 90
                 staircase-middle 90
                 staircase-bottom 90
                 departure-zone 0)
               tag))
        (t 0)))

(defun filler@ (pos)
  (if (null pos)
      (error "Tried to find fillers at NIL.")
      (collect-first (choose-if #'fills-cell-p (scan-body@ pos)))))
(defun departure-zone-p (pos) (find@ 'departure-zone pos))
(defun can-occupy-p (body pos)
  (not (and (fills-cell-p body)
            (filler@ pos))))
(defun climbablep (body pos)
  (declare (ignore body))
  (or (find@ 'staircase-top pos)
      (find@ 'staircase-middle pos)
      (find@ 'staircase-bottom pos)))

(defmethod player ((body positioned)) (player (pos body)))

(defmethod game ((body positioned)) (zone-game (pos-zone (pos body))))
