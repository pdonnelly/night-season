;;; Night Season, a stealth adventure game
;;; Copyright 2019 Paul Donnelly
;;;
;;; This file is part of Night Season.
;;;
;;; Night Season is free software: you can redistribute it and/or modify it
;;; under the terms of the GNU Affero General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.
;;;
;;; Night Season is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
;;; Affero General Public License for more details.
;;;
;;; You should have received a copy of the GNU Affero General Public
;;; License along with Night Season. If not, see
;;; <https://www.gnu.org/licenses/>.

(in-package :night-season)

(defvar *last-turn-submission* 0)
(defun submit-player-turn-parsed (player command)
  (let ((game (game player)))
    (setf *last-turn-submission* (get-internal-real-time))
    (when (zerop (game-turns game))
      (setf (acting-this-turn-p (player game)) t))
    (with-output-to-log game
      (loop until (player-wants-command-p game)
            for i from 0
            do
               (let ((elapsed (- (get-internal-real-time)
                                 *last-turn-submission*)))
                 (when (> elapsed internal-time-units-per-second)
                   (cerror "Continue advancing world?"
                           "The player does not seem to be getting turns. ~a ms have elapsed."
                           (round (* 1000
                                     (/ elapsed
                                        internal-time-units-per-second)))))
                 (advance game nil))
            finally
               (advance game
                        (if (or (null command)
                                (player-wants-command-p game))
                            command
                            (prog1 nil
                              (message t player 'between-turns))))
               (format t "~&(~a subturns)" (1+ i))))))

(defun sim-player-ai ()
  ;; This is a bit silly looking, but we're assuming that the player's
  ;; chosen action is set in ADVANCE and we just need to pass it
  ;; through.
  (command (chosen-action *self*)))

(defun player-wants-command-p (game)
  ;; The player will want a command if they are both acting and not in
  ;; a multi-turn command.
  (acting-this-turn-p (player game)))

(defgeneric %roll-to-act (actor)
  (:method ((actor actor-mixin))
    (setf (acting-this-turn-p actor) (whichever t nil)))
  (:method (a) (declare (ignore a))))

(defun begin-turn (game)
  (incf (game-turns game))
  ;; decide who is acting
  (do-all-bodies (b game)
    (%roll-to-act b))
  (let ((p (player game)))
    (memorize-fov p)
    (setf (visibility p)
          (collect-length (choose (litp (scan-neighbors p t))))))
  (player-wants-command-p game))

(defgeneric roll-initiative (actor)
  (:method ((actor actor-mixin)) (random most-positive-fixnum))
  (:method (b)
    (declare (ignore b))
    nil))

(defun actors-by-initiative (game)
  (sort (let ((actors nil))
          (do-all-bodies (b game)
            (let ((i (roll-initiative b)))
              (when i
                (push (cons i b) actors))))
          actors)
        #'> :key #'car))

(defgeneric choose-action (actor)
  (:method ((a actor-mixin))
    (when (acting-this-turn-p a)
      (setf (chosen-action a) (ai-decide a))))
  (:method (b) (declare (ignore b))))

(defun advance (game command)
  "Complete GAME's current turn. COMMAND is a parsed command for the
  player character to carry out. After completing the turn, rolls are
  made to see which bodies will get to act on the next turn. Returns T
  if the player character wants a command, NIL otherwise. If a command
  is specified but the player character did not want one this turn or
  vice versa, an error is signalled."
  (when (and command (not (player-wants-command-p game)))
    (error "A command was provided when the player character did not want one."))
  (when (and (not command) (player-wants-command-p game))
    (error "Though the player character wanted a command, none was provided."))

  (when command
    (unless (apply #'check-command (player game) command)
      (return-from advance))
    (setf (chosen-action (player game)) command))

  ;; apply gravity
  (let ((may-fall nil))
    ;; Careful, if we fall bodies as we find them, they'll fall to a
    ;; position we subsequently search.
    (do-all-bodies (b game)
      (unless (sceneryp b)
        (push b may-fall)))
    (iterate ((body (scan may-fall)))
      (let ((dest (shift body :z 1)))
        (when (and (not (climbablep body (pos body)))
                   (can-occupy-p body dest))
          (setf (pos body) dest)
          (if (can-occupy-p body (shift dest :z 1))
              (message t body
                       'you-are-falling
                       `(it-is-falling (actor . ,(tag body))))
              (message t body
                       'you-fall
                       `(it-falls (actor . ,(tag body)))))))))

  (do-all-bodies (a game) (choose-action a))
  
  (collect-ignore
   (let* ((a (cdr (scan (actors-by-initiative game))))
          (ac (chosen-action a)))
     (when ac
       (apply #'exec-command a ac)
       (setf (chosen-action a) nil))))
  
  (begin-turn game))

