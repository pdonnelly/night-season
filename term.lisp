;;; Night Season, a stealth adventure game
;;; Copyright 2019 Paul Donnelly
;;;
;;; This file is part of Night Season.
;;;
;;; Night Season is free software: you can redistribute it and/or modify it
;;; under the terms of the GNU Affero General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.
;;;
;;; Night Season is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
;;; Affero General Public License for more details.
;;;
;;; You should have received a copy of the GNU Affero General Public
;;; License along with Night Season. If not, see
;;; <https://www.gnu.org/licenses/>.

(in-package :night-season)

(defclass terminal ()
  ((width :initform 80 :accessor terminal-width)
   (height :initform 24 :accessor terminal-height))
  (:documentation "Base class for all 2d terminals."))

(defclass pseudo-terminal (terminal)
  ((contents :accessor pseudo-terminal-contents))
  (:documentation "A terminal with no real IO capability, but usefully manipulable from the REPL."))

(defgeneric terminal-clear (term)
  (:method (term)
    (setf (pseudo-terminal-contents term)
          (make-array (list
                       (terminal-height term)
                       (terminal-width term))))))

(defgeneric terminal-char-count (term)
  (:method (term)
    (* (terminal-width term)
       (terminal-height term))))

(defgeneric terminal-set (term i char
                          &optional fg bg)
  (:method ((term pseudo-terminal) i char &optional (fg '(1 1 1)) (bg '(0 0 0)))
    (let ((a (pseudo-terminal-contents term)))
      (setf (row-major-aref a i) char))))

(defgeneric terminal-draw (term)
  (:method ((term pseudo-terminal))
    (pseudo-terminal-contents term)))
(defgeneric terminal-cleanup (term)
  (:documentation "Do things like freeing resources when done with TERM, which mustn't be used after."))

(defmacro with-terminal (class var &body body)
  (let ((%var (gensym "term")))
    `(let (,%var)
       (unwind-protect
            (progn (setf ,var
                         (setf ,%var (make-instance ,class)))
                   ,@body)
         (terminal-cleanup ,%var)))))
