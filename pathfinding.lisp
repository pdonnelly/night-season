;;; Night Season, a stealth adventure game
;;; Copyright 2019 Paul Donnelly
;;;
;;; This file is part of Night Season.
;;;
;;; Night Season is free software: you can redistribute it and/or modify it
;;; under the terms of the GNU Affero General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.
;;;
;;; Night Season is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
;;; Affero General Public License for more details.
;;;
;;; You should have received a copy of the GNU Affero General Public
;;; License along with Night Season. If not, see
;;; <https://www.gnu.org/licenses/>.

(in-package :night-season)

(defstruct (path (:print-function print-path))
  pos (previous nil) (cost-so-far 0) (estimated-cost 0))

(defun print-path (path &optional (stream t) depth)
  (declare (ignore depth))
  (format stream "#<Path to ~a cost ~,1f>"
          (path-pos path) (path-estimated-cost path)))

(defun unroll-path (path)
  (let ((unrolled nil))
    (do ((s path (path-previous s)))
        ((null s) unrolled)
      (push (path-pos s) unrolled))))

(defun extend-path (path pos estimate-to-goal)
  (let ((cost-so-far (+ (path-cost-so-far path)
                        1)))
    (make-path :pos pos :previous path :cost-so-far cost-so-far
               :estimated-cost (+ cost-so-far estimate-to-goal))))

(defun passablep (me pos)
  (null
   (collect-first
    (choose-if (lambda (b) (and (not (eq me b))
                                (fills-cell-p b)))
               (scan-body@ pos)))))

(defstruct (a*-state (:constructor %make-a*-state))
  start goal open best-paths)

(defun make-a*-state (start goal)
  (let ((s (%make-a*-state)))
    (with-slots ((path-start start) (path-goal goal) open best-paths) s
      (setf path-start start
            path-goal goal
            open (list (make-path :pos start))
            best-paths (make-array (mapcar (lambda (n) (+ n 2))
                                           (zone-dimensions (zone start)))
                                   :initial-element nil))
      (set-best-path s start (first open)))
    s))

(defun get-best-path (state pos)
  (pos-aref (a*-state-best-paths state) (shift pos :z 1 :y 1 :x 1)))

(defun set-best-path (state pos path)
  (setf (pos-aref (a*-state-best-paths state) (shift pos :z 1 :y 1 :x 1))
        path))

(defun possible-walk-p (start dest)
  ;; A walk is possible if a mob in the starting cell can plausibly
  ;; move that way, regardless of whether dest is blocked or not. What
  ;; we want to filter out are moves which require flying. A good
  ;; approximation is to consider a walk possible if both START and
  ;; DEST are positions with either floor below or stairs in.
  (and (not (pos= start dest))
       (or (climbablep nil start)
           (not (can-occupy-p nil (shift start :z 1))))
       (or (climbablep nil dest)
           (not (can-occupy-p nil (shift dest :z 1))))))


(defun %update-a* (s considering)
  (let ((start (path-pos considering)))
    (iterate ((pos (choose-if (lambda (n) (possible-walk-p start n))
                              (scan-neighbors start))))
      (let* ((goal (a*-state-goal s))
             (path (extend-path considering pos (dist pos goal)))
             (old-path (get-best-path s pos))
             (goal-path (get-best-path s goal)))
        (when (or (and (null old-path)
                       (or (null goal-path)
                           (< (path-estimated-cost path)
                              (path-estimated-cost goal-path))))
                  (and old-path
                       (< (path-estimated-cost path)
                          (path-estimated-cost old-path))))
          (push path (a*-state-open s))
          (set-best-path s pos path))))))

(defun array-a* (me start goal continue-state max-iterations)
  (when (and continue-state (or start goal))
    (error "Can't supply both a state to continue and a start or goal."))
  (unless (or continue-state
              (and start goal))
    (return-from array-a* nil))
  (loop
     with s = (or continue-state (make-a*-state start goal))
     for i from 0 do
       (let ((considering (pop (a*-state-open s))))
         (cond ((>= i max-iterations)
                (return (values nil s)))
               ((null considering)
                (return (values (unroll-path (get-best-path s (a*-state-goal s)))
                                nil)))
               ((passablep me (path-pos considering))
                (%update-a* s considering))))))

(defun pathfind (me start goal max-ms &optional continue-state)
  (array-a* me start goal continue-state max-ms))
